package com.mkrzanowsgames.semagswonazrkm.fingertris.framework.implementation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Audio;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.FileIO;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;

public abstract class AndroidGame extends Activity implements Game {
	private static final String AD_UNIT_ID = "ca-app-pub-7480115400347003/7614839574";
	RelativeLayout layout;
	AdView helper;
	AdView adView;
	
	AndroidFastRenderView renderView;
	Graphics graphics;
	Audio audio;
	Input input;
	FileIO fileIO;
	Screen screen;
	WakeLock wakeLock;
	InputMethodManager imm;
	RelativeLayout.LayoutParams lp;
	boolean asyncHiding;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM,
		// WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

		boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
		int frameBufferWidth = isPortrait ? 800 : 1280;
		int frameBufferHeight = isPortrait ? 1280 : 800;
		Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
				frameBufferHeight, Config.RGB_565);

		float scaleX = (float) frameBufferWidth
				/ getWindowManager().getDefaultDisplay().getWidth();
		float scaleY = (float) frameBufferHeight
				/ getWindowManager().getDefaultDisplay().getHeight();

		asyncHiding = false;
		layout = new RelativeLayout(this);
		adView = new AdView(this);
		helper = new AdView(this);
		renderView = new AndroidFastRenderView(this, frameBuffer);
		graphics = new AndroidGraphics(getAssets(), frameBuffer);
		fileIO = new AndroidFileIO(this);
		audio = new AndroidAudio(this);
		input = new AndroidInput(this, renderView, scaleX, scaleY);
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		screen = getInitScreen();

		/** Ad setup space */

		helper.setAdSize(AdSize.BANNER);
		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(AD_UNIT_ID);
		adView.setBackgroundColor(Color.BLACK);

		lp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		adView.setLayoutParams(lp);
		
		helper.setLayoutParams(lp);

		AdRequest adRequest = new AdRequest.Builder()
				.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				.addTestDevice("07F3CEC0A7175BB821DBEC93215D6B3A").build();

		// Start loading the ad in the background.
		adView.loadAd(adRequest);

		/***/
		layout.addView(renderView);
		layout.addView(adView);
		layout.addView(helper);
		helper.requestFocus();
		helper.setFocusableInTouchMode(true);

		setContentView(layout);

		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,
				"MyGame");
	}

	private class hideAdTask extends AsyncTask<Void, String, Long> {
		@Override
		protected void onPreExecute() {
			if (asyncHiding) {
				cancel(false);
			} else {
				asyncHiding = true;
			}
		}

		@Override
		protected Long doInBackground(Void... params) {
			publishProgress("");
			return null;
		};

		protected void onProgressUpdate(String... progress) {
			super.onProgressUpdate(progress);
			if (adView.getVisibility() == View.VISIBLE) {
				adView.setVisibility(View.GONE);
				layout.removeView(adView);
			}
		}

	}

	private class showAdTask extends AsyncTask<Void, String, Long> {
		@Override
		protected void onPreExecute() {
			if (!asyncHiding) {
				cancel(false);
			} else {
				asyncHiding = false;
			}
		}

		@Override
		protected Long doInBackground(Void... params) {
			publishProgress("");
			return null;
		};

		protected void onProgressUpdate(String... progress) {
			super.onProgressUpdate(progress);
			if (adView.getVisibility() == View.GONE) {
				adView.setVisibility(View.VISIBLE);
				layout.addView(adView, lp);
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) != ConnectionResult.SUCCESS) {
			GooglePlayServicesUtil.getErrorDialog(
					GooglePlayServicesUtil.isGooglePlayServicesAvailable(this),
					this, ConnectionResult.SUCCESS);
		}

		wakeLock.acquire();
		screen.resume();
		renderView.resume();
		adView.resume();

	}

	@Override
	public IBinder getWindowToken() {
		return renderView.getWindowToken();

	}

	@SuppressWarnings("deprecation")
	@Override
	public void hideAd() {
		if (getWindowManager().getDefaultDisplay().getWidth() / 2 < adView
				.getWidth())
			new hideAdTask().execute();

	}

	@SuppressWarnings("deprecation")
	@Override
	public void showAd() {
		if (getWindowManager().getDefaultDisplay().getWidth() / 2 < adView
				.getWidth())
			new showAdTask().execute();

	}

	@Override
	public void onPause() {
		super.onPause();

		adView.pause();
		wakeLock.release();
		renderView.pause();
		screen.pause();

		if (isFinishing())
			screen.dispose();
	}

	@Override
	public InputMethodManager getInputMethodManager() {
		return imm;
	}

	@Override
	public Input getInput() {
		return input;
	}

	@Override
	public FileIO getFileIO() {
		return fileIO;
	}

	@Override
	public Graphics getGraphics() {
		return graphics;
	}

	@Override
	public Audio getAudio() {
		return audio;
	}

	@Override
	public void setScreen(Screen screen) {
		if (screen == null)
			throw new IllegalArgumentException("Screen must not be null");

		this.screen.pause();
		this.screen.dispose();
		screen.resume();
		screen.update(0);
		this.screen = screen;
	}

	public Screen getCurrentScreen() {

		return screen;
	}

	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		screen.dispose();
		super.onDestroy();
	}

}