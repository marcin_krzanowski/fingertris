package com.mkrzanowsgames.semagswonazrkm.fingertris.data;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.mkrzanowsgames.semagswonazrkm.fingertris.start.Fingertris;

public class Data {
	private class score {
		public double value;
		public String name;

		public score() {
			this.value = 0;
			this.name = "AAA";
		}

		public score(String name, double value) {
			set(name, value);
		}

		public void set(String name, double value) {
			this.value = value;
			this.name = name;
		}

		public score getScore() {
			return new score(this.name, this.value);
		}

	}

	// const
	public static final int gameFieldOffset = 100;
	public static final int gameFieldWidth = 500;
	public static final int gameFieldHeight = 800;
	public static final int blockSize = 50;
	public static final int blockSize2 = 50;
	public static final int buttOffset = 60;
	public static final int dispOffset = 350;
	public static final int dispTouchOffset = 20;
	public static final int buttSize = 190;
	public static final int pauseButtX = 620;
	public static final int scoreOffset = 30;
	public static final double timeLen = 570.0;

	// rest
	private SharedPreferences prefs;
	private int level;
	private int maxLevel;
	private score hiScore[];
	private String lastName;
	private float soundVolumeMusic;
	private float soundVolumeSound;
	private boolean musicOn;
	private boolean soundOn;

	public Data() {
		this.level = 0;
		this.maxLevel = 0;
		this.setSoundVolumeMusic((float) 0.5);
		this.setSoundVolumeSound((float) 0.5);
		this.musicOn = true;
		this.soundOn = true;
		this.lastName = "AAA";
		this.hiScore = new score[10];
		clearHiScore();

	}

	private void clearHiScore() {
		for (int i = 0; i < 10; i++) {
			hiScore[i] = new score();
		}

	}

	public Data(Fingertris game) {
		prefs = PreferenceManager.getDefaultSharedPreferences(game);

		this.level = prefs.getInt("level", 0);
		this.maxLevel = prefs.getInt("maxLevel", 0);
		this.setSoundVolumeMusic(prefs
				.getFloat("soundVolumeMusic", (float) 0.5));
		this.setSoundVolumeSound(prefs
				.getFloat("soundVolumeSound", (float) 0.5));
		this.musicOn = prefs.getBoolean("musicOn", true);
		this.soundOn = prefs.getBoolean("soundOn", true);
		this.lastName = prefs.getString("lastName", "AAA");
		this.hiScore = new score[10];

		for (int i = 0; i < 10; i++) {
			hiScore[i] = new score(prefs.getString(
					"hiscoreName" + String.valueOf(i), "AAA"),
					(double) prefs.getLong("hiscoreValue" + String.valueOf(i),
							0));
		}

	}

	public void save() {
		Editor editor = prefs.edit();

		editor.putInt("level", this.level);
		editor.putInt("maxLevel", this.maxLevel);
		editor.putFloat("soundVolumeMusic", getSoundVolumeMusic());
		editor.putFloat("soundVolumeSound", getSoundVolumeSound());
		editor.putBoolean("musicOn", this.musicOn);
		editor.putBoolean("soundOn", this.soundOn);
		editor.putString("lastName", this.lastName);

		for (int i = 0; i < 10; i++) {
			editor.putString("hiscoreName" + String.valueOf(i), hiScore[i].name);
			editor.putLong("hiscoreValue" + String.valueOf(i),
					(long) hiScore[i].value);
		}

		editor.commit();
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public double getHiScore() {
		return hiScore[0].value;
	}

	public double getLoScore() {
		return hiScore[9].value;
	}

	public boolean isMusicOn() {
		return musicOn;
	}

	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public void setMusicOn(boolean musicOn) {
		this.musicOn = musicOn;
	}

	public boolean isSoundOn() {
		return soundOn;
	}

	public void setSoundOn(boolean soundOn) {
		this.soundOn = soundOn;
	}

	public float getSoundVolumeMusic() {
		return soundVolumeMusic;
	}

	public float getSoundVolumeSound() {
		return soundVolumeSound;
	}

	public void setSoundVolumeSound(float soundVolume) {
		this.soundVolumeSound = soundVolume;
	}

	public void setSoundVolumeMusic(float soundVolume) {
		this.soundVolumeMusic = soundVolume;
	}

	public double getScoreValue(int pos) {
		return hiScore[pos].value;
	}

	public String getScoreName(int pos) {
		return hiScore[pos].name;
	}

	public void addScore(double value, String name) {
		score temp[] = new score[10];
		for (int i = 0; i < 10; i++) {
			temp[i] = hiScore[i].getScore();
		}

		for (int i = 0; i < 10; i++) {
			if (hiScore[i].value <= value) {
				for (int j = i + 1; j < 10; j++) {
					hiScore[j] = temp[j - 1];
				}
				hiScore[i].set(name, value);
				return;
			}
		}
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
