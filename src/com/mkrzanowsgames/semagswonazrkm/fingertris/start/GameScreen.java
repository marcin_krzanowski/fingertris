package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.graphics.Color;
import android.graphics.Paint;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Data;
import com.mkrzanowsgames.semagswonazrkm.fingertris.data.colourList;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.Bin;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.Block;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.BlockDispenser;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.GameField;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.Block.BlockState;

public class GameScreen extends Screen {
	enum GameState {
		Ready, Running, Paused, GameOver
	}

	public static colourList colourList;
	private GameState state;
	// Variable setup, game objects

	public static int level;
	public static double score;
	private int[] treshold;
	public static int[] scoreBonus;
	private double hiScore;
	private double tempScore;
	private boolean stop;
	private boolean now;
	public static float timer;
	private static float maxTimer;
	public static Paint pScore, pHiScore;
	private GameField gameField;
	private BlockDispenser left, center, right, extra;
	public static ArrayList<Block> blockList;
	public static ArrayList<Block> draggedListOnField;
	public static ArrayList<Block> draggedListReady;
	public Bin bin;
	private int gameOverTimer;

	public GameScreen(Game game) {
		super(game);
		game.hideAd();
		Assets.splash = null;

		colourList = new colourList();

		// Initialize game objects here

		// Defining paint object
		pScore = new Paint();
		pScore.setTextSize(50);
		pScore.setTextAlign(Paint.Align.CENTER);
		pScore.setAntiAlias(true);
		pScore.setColor(Color.BLACK);
		pScore.setTypeface(Assets.Gi);

		pHiScore = new Paint();
		pHiScore.setTextSize(50);
		pHiScore.setTextAlign(Paint.Align.CENTER);
		pHiScore.setAntiAlias(true);
		pHiScore.setColor(Color.BLACK);
		pHiScore.setTypeface(Assets.Gi);

		left = new BlockDispenser(true, 1);
		center = new BlockDispenser(true, 2);
		right = new BlockDispenser(true, 3);
		extra = new BlockDispenser();
		gameField = new GameField();

		tempScore = score = 0.0;
		now = false;
		bin = new Bin();
		blockList = new ArrayList<Block>();
		draggedListOnField = new ArrayList<Block>();
		draggedListReady = new ArrayList<Block>();
		level = Fingertris.data.getLevel();
		state = GameState.Ready;
		hiScore = Fingertris.data.getHiScore();
		timer = 0;
		gameOverTimer = -1;

		setMaxTimer();

		treshold = new int[10];
		treshold[0] = 55;
		treshold[1] = 130;
		treshold[2] = 250;
		treshold[3] = 380;
		treshold[4] = 550;
		treshold[5] = 1300;
		treshold[6] = 3100;
		treshold[7] = 8200;
		treshold[8] = 20500;
		treshold[9] = 100000;

		scoreBonus = new int[10];
		scoreBonus[0] = 1;
		scoreBonus[1] = 2;
		scoreBonus[2] = 4;
		scoreBonus[3] = 6;
		scoreBonus[4] = 10;
		scoreBonus[5] = 20;
		scoreBonus[6] = 40;
		scoreBonus[7] = 100;
		scoreBonus[8] = 200;
		scoreBonus[9] = 400;

	}

	public static void setMaxTimer() {
		switch (level) {
		case 1:
			maxTimer = 550;
			return;
		case 2:
			maxTimer = 480;
			return;
		case 3:
			maxTimer = 400;
			return;
		case 4:
			maxTimer = 310;
			return;
		case 5:
			maxTimer = 220;
			return;
		case 6:
			maxTimer = 180;
			return;
		case 7:
			maxTimer = 145;
			return;
		case 8:
			maxTimer = 115;
			return;
		case 9:
			maxTimer = 100;
			return;
		case 0:
		default:
			maxTimer = 600;
			return;
		}
	}

	@Override
	public void update(float deltaTime) {
		if (level < 0) {
			state = GameState.GameOver;
		} else {
			if ((score > tempScore + treshold[level]) && level < 9) {
				Assets.lvlUp.play(Fingertris.data.getSoundVolumeSound());
				tempScore = score;
				level++;
				setMaxTimer();

			}
			/*
			 * else if (score > tempScore + treshold[level] && level == 9) {
			 * Assets.lvlUp.play(Fingertris.data.getSoundVolumeSound()); score
			 * += 50000; tempScore = score;
			 * 
			 * }
			 */
		}

		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		switch (state) {
		case GameOver:
			updateGameOver(touchEvents);
			break;
		case Paused:
			updatePaused(touchEvents);
			break;
		case Ready:
			updateReady(touchEvents, deltaTime);
			break;
		case Running:
			updateRunning(touchEvents, deltaTime);
			break;
		default:
			break;

		}

	}

	private void updateReady(List<TouchEvent> touchEvents, float deltaTime) {
		// Here goes "Ready" screen, and it's handling

		if (touchEvents.size() > 0) {
			state = GameState.Running;
			left.update(deltaTime, true, true);
			center.update(deltaTime, true, true);
			right.update(deltaTime, true, true);
		}

	}

	private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
		// Here every object is updated, and also all additional features
		// are checked

		if (timer < maxTimer) {
			timer += deltaTime;
		} else {
			now = true;
			timer = 0;
		}

		if (left.update(deltaTime, now, false)) {
			now = false;
			center.update(deltaTime, now, false);
			right.update(deltaTime, now, false);
		} else {
			if (center.update(deltaTime, now, false)) {
				now = false;
				right.update(deltaTime, now, false);
			} else {
				if (right.update(deltaTime, now, false)) {
					now = false;
				} else {
					Assets.lvlDown.play(Fingertris.data.getSoundVolumeSound());
					placeBlock(extra.generateBlock(true), 1, 5,
							new ArrayList<Block>());
					now = false;
					/*
					 * left.center = center.center; center.center =
					 * right.center; right.ready = true; right.center = null;
					 * right.update(deltaTime, now, true);
					 * 
					 * level--; tempScore = score; now = false;
					 */
				}
			}
		}

		if (blockList != null) {
			for (int i = 0; i < blockList.size(); i++) {
				blockList.get(i).free = true;
				blockList.get(i).update(deltaTime);
			}
		}

		// 1. Firstly, all touch input is handled
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			event.free = true;
			stop = false;

			for (int j = 0; j < draggedListReady.size(); j++) {
				if (event.pointer == draggedListReady.get(j).touchPointer) {
					draggedListReady.get(j).x = event.x;
					draggedListReady.get(j).y = event.y;
					event.free = false;
					break;

				}
			}

			// game field handling, for every field of the game field
			for (int j = -1; j < 11; j++) {
				for (int k = 0; k < 17; k++) {
					if (inBounds(event, Data.gameFieldOffset + j
							* Data.blockSize, k * Data.blockSize,
							Data.blockSize, Data.blockSize)) {
						if (event.type == TouchEvent.TOUCH_DOWN && j >= 0
								&& j < 10 && k < 16
								&& GameField.fieldFree[k][j] == false) {
							// if there is a block
							moveBlock(k, j, event.pointer);
							stop = true;

						} else if (event.type == TouchEvent.TOUCH_DOWN
								&& j >= 0 && j < 10 && k < 16
								&& GameField.fieldFree[k][j]) {
							// else find the block nearest to the touch

							double dist = 1000.0;
							int tempj = -1;
							int tempk = -1;
							double tempd;
							if (k - 1 >= 0 && !GameField.fieldFree[k - 1][j]) {
								tempd = java.lang.Math
										.sqrt(((((k - 1) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j;
									tempk = k - 1;
								}
							}
							if (k + 1 < 16 && !GameField.fieldFree[k + 1][j]) {
								tempd = java.lang.Math
										.sqrt(((((k + 1) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j;
									tempk = k + 1;
								}
							}
							if (k - 1 >= 0 && j - 1 >= 0
									&& !GameField.fieldFree[k - 1][j - 1]) {
								tempd = java.lang.Math
										.sqrt(((((k - 1) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j - 1) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j - 1;
									tempk = k - 1;
								}
							}
							if (k - 1 >= 0 && j + 1 < 10
									&& !GameField.fieldFree[k - 1][j + 1]) {
								tempd = java.lang.Math
										.sqrt(((((k - 1) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j + 1) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j + 1;
									tempk = k - 1;
								}
							}
							if (k + 1 < 16 && j - 1 >= 0
									&& !GameField.fieldFree[k + 1][j - 1]) {
								tempd = java.lang.Math
										.sqrt(((((k + 1) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j - 1) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j - 1;
									tempk = k + 1;
								}
							}
							if (k + 1 < 16 && j + 1 < 10
									&& !GameField.fieldFree[k + 1][j + 1]) {
								tempd = java.lang.Math
										.sqrt(((((k + 1) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j + 1) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j + 1;
									tempk = k + 1;
								}
							}
							if (j - 1 >= 0 && !GameField.fieldFree[k][j - 1]) {
								tempd = java.lang.Math
										.sqrt(((((k) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j - 1) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j - 1;
									tempk = k;
								}
							}
							if (j + 1 < 10 && !GameField.fieldFree[k][j + 1]) {
								tempd = java.lang.Math
										.sqrt(((((k) - 1) * Data.blockSize + Data.blockSize / 2) - event.y)
												^ 2
												+ ((Data.gameFieldOffset
														+ ((j + 1) - 1)
														* Data.blockSize + Data.blockSize / 2) - event.x)
												^ 2);
								if (tempd < dist) {
									dist = tempd;
									tempj = j + 1;
									tempk = k;
								}
							}

							if (tempj != -1 && tempk != -1) {
								moveBlock(tempk, tempj, event.pointer);
								stop = true;
							}

						} else if (j > -1
								&& event.x > Data.gameFieldOffset
										+ (j * Data.blockSize)
								&& event.y > k * Data.blockSize
								&& event.x < Data.gameFieldOffset + (j + 1)
										* Data.blockSize
								&& event.y < (k + 1) * Data.blockSize) {
							for (int z = 0; z < draggedListOnField.size(); z++) {
								if (draggedListOnField.get(z).touchPointer == event.pointer) {
									if (blockFits(draggedListOnField.get(z), k,
											j)) {
										freeBlock(draggedListOnField.get(z));
										locateBlock(draggedListOnField.get(z),
												k, j);
										draggedListOnField.get(z).free = false;
										event.free = false;
										stop = true;
										break;
									} else {
										stopBlock(z);
										stop = true;
										break;
									}

								}
							}
						}
						if (event.type == TouchEvent.TOUCH_UP) {
							for (int z = 0; z < draggedListOnField.size(); z++) {
								if (draggedListOnField.get(z).touchPointer == event.pointer) {
									if (blockFits(draggedListOnField.get(z), k,
											j)) {
										freeBlock(draggedListOnField.get(z));
										locateBlock(draggedListOnField.get(z),
												k, j);
										stopBlock(z);
										stop = true;
										break;
									} else {
										stopBlock(z);
										stop = true;
										break;
									}
								}
							}
						}
					}
				}
			}
			if (stop)
				continue;

			/*
			 * // game field handling, for every field of the game field for
			 * (int j = -1; j < 11; j++) { for (int k = 0; k < 17; k++) { if
			 * (inBounds(event, 200 + j * 50, k * 50, 50, 50)) { if (event.type
			 * == TouchEvent.TOUCH_DOWN && j >= 0 && j < 10 && k < 16 &&
			 * GameField.fieldFree[k][j] == false) { moveBlock(k, j); stop =
			 * true; } else if (event.type == TouchEvent.TOUCH_DRAGGED &&
			 * event.free) { for (int z = 0; z < draggedListOnField.size(); z++)
			 * { if (draggedListOnField.get(z).free && inBounds( event,
			 * draggedListOnField.get(z).x - 150, draggedListOnField.get(z).y -
			 * 150, 250, 250)) { if (blockFits(draggedListOnField.get(z), k, j))
			 * { freeBlock(draggedListOnField.get(z));
			 * locateBlock(draggedListOnField.get(z), k, j);
			 * draggedListOnField.get(z).free = false; event.free = false; stop
			 * = true; break; } else { stopBlock(z); stop = true; break; }
			 * 
			 * } } } else if (event.type == TouchEvent.TOUCH_UP) { for (int z =
			 * 0; z < draggedListOnField.size(); z++) { if (inBounds(event,
			 * draggedListOnField.get(z).x - 125, draggedListOnField.get(z).y -
			 * 125, 250, 250)) { if (blockFits(draggedListOnField.get(z), k, j))
			 * { freeBlock(draggedListOnField.get(z));
			 * locateBlock(draggedListOnField.get(z), k, j); stopBlock(z); stop
			 * = true; break; } else { stopBlock(z); stop = true; break; } } } }
			 * } } } if (stop) continue;
			 */
			if (event.type == TouchEvent.TOUCH_DOWN) {
				// other events handling, like a pause button, but not forfeit
				// button
				if (inBounds(event, Data.pauseButtX, Data.buttOffset,
						Data.buttSize, Data.buttSize)) {
					if (state == GameScreen.GameState.Paused) {
						if (state == GameState.Paused) {
							state = GameState.Running;
						}
						stop = true;
					} else {
						pause();
						stop = true;
					}
				}
				if (stop)
					continue;

				if (inBounds(event, Data.pauseButtX + 2 * Data.buttSize,
						Data.buttOffset, Data.buttSize, Data.buttSize)) {
					if (left.ready || center.ready || right.ready) {
						now = true;
						score += scoreBonus[level];
						timer = 0;
						stop = true;
					}
				}
				if (stop)
					continue;

				if (inBounds(event, Data.pauseButtX, Data.dispOffset,
						Data.buttSize, Data.buttSize) && left.center != null) {
					left.touched = true;
					left.center.touchPointer = event.pointer;
					stop = true;
				}
				if (stop)
					continue;

				if (inBounds(event, Data.pauseButtX + Data.buttSize,
						Data.dispOffset, Data.buttSize, Data.buttSize)
						&& center.center != null) {
					center.touched = true;
					center.center.touchPointer = event.pointer;
					stop = true;

				}
				if (stop)
					continue;

				if (inBounds(event, Data.pauseButtX + 2 * Data.buttSize,
						Data.dispOffset, Data.buttSize, Data.buttSize)
						&& right.center != null) {
					right.touched = true;
					right.center.touchPointer = event.pointer;
					stop = true;
				}

			}
			if (stop)
				continue;

			// TODO better dragging handling (DONE?)

			/*
			 * if (blockList != null) { for (int j = 0; j < blockList.size();
			 * j++) { if (blockList.get(j).onTouch && blockList.get(j).state ==
			 * BlockState.Ready && event.free && blockList.get(j).free) { if
			 * (inBounds(event, blockList.get(j).x - 250, blockList.get(j).y -
			 * 250, 525, 525)) { blockList.get(j).changCoords(event.x, event.y);
			 * event.free = false; blockList.get(j).free = false; stop = true;
			 * break; } } } }
			 * 
			 * if (stop) continue;
			 */

			// dispensers handling
			if ((left.touched && left.center.touchPointer == event.pointer)
					&& (event.x < Data.pauseButtX + Data.dispTouchOffset
							|| event.x > Data.pauseButtX - Data.dispTouchOffset
									+ Data.buttSize
							|| event.y < Data.dispOffset + Data.dispTouchOffset || event.y > Data.dispOffset
							- Data.dispTouchOffset + Data.buttSize)) {
				if (left.ready) {
				} else {
					left.center.onTouch = true;
					draggedListReady.add(left.center);
					left.ready = true;
					left.center = null;
					event.free = false;
					left.touched = false;
					stop = true;
				}
			} else if ((center.touched && center.center.touchPointer == event.pointer)
					&& (event.x < Data.pauseButtX + Data.dispTouchOffset
							+ Data.buttSize
							|| event.x > Data.pauseButtX - Data.dispTouchOffset
									+ 2 * Data.buttSize
							|| event.y < Data.dispOffset + Data.dispTouchOffset || event.y > Data.dispOffset
							- Data.dispTouchOffset + Data.buttSize)) {
				if (center.ready) {
				} else {
					center.center.onTouch = true;
					draggedListReady.add(center.center);
					center.ready = true;
					center.center = null;
					event.free = false;
					center.touched = false;
					stop = true;
				}
			} else if ((right.touched && right.center.touchPointer == event.pointer)
					&& (event.x < Data.pauseButtX + Data.dispTouchOffset + 2
							* Data.buttSize
							|| event.x > Data.pauseButtX - Data.dispTouchOffset
									+ 3 * Data.buttSize
							|| event.y < Data.dispOffset + Data.dispTouchOffset || event.y > Data.dispOffset
							- Data.dispTouchOffset + Data.buttSize)) {
				if (right.ready) {
				} else {
					right.center.onTouch = true;
					draggedListReady.add(right.center);
					right.ready = true;
					right.center = null;
					event.free = false;
					right.touched = false;
					stop = true;
				}

			}

			if (stop)
				continue;

			if (event.type == TouchEvent.TOUCH_UP) {

				for (int j = 0; j < draggedListReady.size(); j++) {
					if (draggedListReady.get(j).touchPointer == event.pointer) {
						draggedListReady.get(j).onTouch = false;
						draggedListReady.get(j).touchPointer = -1;
						// if is dropped on game field...
						if (inBounds(event, Data.gameFieldOffset, 0,
								Data.gameFieldWidth, Data.gameFieldHeight)) {
							for (int z = 0; z < 10; z++) {
								for (int k = 0; k < 16; k++) {
									if (inBounds(event, Data.gameFieldOffset
											+ z * Data.blockSize, k
											* Data.blockSize, Data.blockSize,
											Data.blockSize)) {
										placeBlock(draggedListReady.get(j), k,
												z, new ArrayList<Block>());
										stop = true;
										break;
									}
								}
							}
							// if is dropped into the bin
						} else if (inBounds(event, Data.pauseButtX
								+ Data.blockSize, Data.dispOffset
								+ Data.buttSize + Data.blockSize, 570, 400)
								&& draggedListReady.get(j).state != BlockState.OnField) {
							if (bin.recycle(j)) {
							} else {
								findPlace(j);
								stop = true;
							}
							// if in left side of field, near edge
						} else if (inBounds(event, Data.gameFieldOffset
								- Data.blockSize, 0, Data.blockSize,
								Data.gameFieldHeight)) {
							placeBlock(draggedListReady.get(j), event.y / 50,
									0, new ArrayList<Block>());

							// or, anywhere else
						} else if (draggedListReady.get(j).state != BlockState.OnField) {
							findPlace(j);
							stop = true;
						}
						break;
					}
				}
				if (stop)
					continue;

				if (inBounds(event, Data.pauseButtX, Data.dispOffset,
						Data.buttSize, Data.buttSize)) {
					if (left.center != null && left.touched) {
						left.center.flip();
						left.center.touchPointer = -1;
						left.touched = false;
						stop = true;
					}
				}
				if (stop)
					continue;

				if (inBounds(event, Data.pauseButtX + Data.buttSize,
						Data.dispOffset, Data.buttSize, Data.buttSize)) {
					if (center.center != null && center.touched) {
						center.center.flip();
						center.center.touchPointer = -1;
						center.touched = false;
						stop = true;
					}
				}
				if (stop)
					continue;

				if (inBounds(event, Data.pauseButtX + 2 * Data.buttSize,
						Data.dispOffset, Data.buttSize, Data.buttSize)) {
					if (right.center != null && right.touched) {
						right.center.flip();
						right.center.touchPointer = -1;
						right.touched = false;
						stop = true;
					}
				}

			}
			if (stop)
				continue;

		}
		gameField.update(deltaTime);
		// 2. Check miscellaneous events

		// 3. Call individual update methods here
		if (blockList != null) {
			for (int i = 0; i < blockList.size(); i++) {
				blockList.get(i).update(deltaTime);
			}
		}
		bin.update();

	}

	private void freeBlock(Block block) {
		ArrayList<Block> mapa = block.map(new ArrayList<Block>(),
				block.vertical, block.horizontal);
		for (int j = 0; j < mapa.size(); j++) {
			GameField.fieldFree[mapa.get(j).vertical][mapa.get(j).horizontal] = true;
		}

	}

	private void locateBlock(Block block, int vertical, int horizontal) {
		ArrayList<Block> mapa = block.map(new ArrayList<Block>(), vertical,
				horizontal);
		for (int j = 0; j < mapa.size(); j++) {
			GameField.fieldFree[mapa.get(j).vertical][mapa.get(j).horizontal] = false;
		}

	}

	private void stopBlock(int z) {

		ArrayList<Block> mapa = draggedListOnField.get(z).map(
				new ArrayList<Block>(), draggedListOnField.get(z).vertical,
				draggedListOnField.get(z).horizontal);
		draggedListOnField.get(z).touchPointer = -1;
		draggedListOnField.remove(z);
		for (int j = 0; j < mapa.size(); j++) {
			if (mapa.get(j).isCenter) {
				mapa.get(j).onTouch = false;
				return;
			}

		}

	}

	private boolean blockFits(Block center, int vertical, int horizontal) {

		freeBlock(center);
		ArrayList<Block> mapa = center.map_n(new ArrayList<Block>(), vertical,
				horizontal);

		int len = mapa.size();
		for (int i = 0; i < len; i++) {
			if (mapa.get(i).vertical < 0 || mapa.get(i).vertical > 15
					|| mapa.get(i).horizontal < 0 || mapa.get(i).horizontal > 9) {
				locateBlock(center, center.vertical, center.horizontal);
				return false;
			} else if (GameField.fieldFree[mapa.get(i).vertical][mapa.get(i).horizontal] == false) {
				locateBlock(center, center.vertical, center.horizontal);
				return false;
			}
		}
		locateBlock(center, center.vertical, center.horizontal);
		return true;

	}

	private void moveBlock(int vertical, int horizontal, int pointer) {
		int len = blockList.size();
		for (int i = 0; i < len; i++) {
			if (blockList.get(i).vertical == vertical
					&& blockList.get(i).horizontal == horizontal
					&& blockList.get(i).state == Block.BlockState.OnField) {
				draggedListOnField.add(blockList.get(i));
				blockList.get(i).touchPointer = pointer;
				if (blockList.get(i).isCenter) {
					blockList.get(i).onTouch = true;

				} else {
					ArrayList<Block> mapa = blockList.get(i).map(
							new ArrayList<Block>(), vertical, horizontal);
					for (int j = 0; j < mapa.size(); j++) {
						if (mapa.get(j).isCenter) {
							mapa.get(j).onTouch = true;
							return;
						}

					}
				}
			}
		}

	}

	private void findPlace(int j) {
		Block centerBlock = draggedListReady.remove(j);
		// centerBlock = blockList.get(j);
		if (left.ready) {
			left.ready = false;
			left.center = (centerBlock);
		} else {
			if (center.ready) {
				center.ready = false;
				center.center = centerBlock;
			} else {
				if (right.ready) {
					right.ready = false;
					right.center = centerBlock;
				} else {
					if (bin.recycle(centerBlock)) {
					} else {
						if (centerBlock.x > Data.gameFieldOffset
								+ Data.gameFieldWidth / 2) {
							if (centerBlock.y > Data.gameFieldHeight / 2) {
								placeBlock(centerBlock, 15, 9,
										new ArrayList<Block>());
							} else {
								placeBlock(centerBlock, 0, 9,
										new ArrayList<Block>());
							}
						} else {
							if (centerBlock.y > Data.gameFieldHeight / 2) {
								placeBlock(centerBlock, 15, 0,
										new ArrayList<Block>());
							} else {
								placeBlock(centerBlock, 0, 0,
										new ArrayList<Block>());
							}
						}
					}
				}
			}
		}

	}

	private void placeBlock(Block centerBlock, int vertical, int horizontal,
			ArrayList<Block> map) {
		int verticK = 0;
		int horizJ = 0;

		map = centerBlock.map(map, 0, 0);

		if (GameField.fieldFree[vertical + verticK][horizontal + horizJ]) {
			if (insertBlock(centerBlock, vertical + verticK, horizontal
					+ horizJ, map)) {
				return;
			}
		}

		for (double r = 1; r < 16; r++) {
			double tempa = java.lang.Math.ceil((1 + 2 * (r - 1)) / 2.0);
			double tempb = 0;
			double tempc = 0;
			double tempd = tempa - 1;
			double tempe = 0;
			double tempf = tempa;
			for (double z = 0; z < r * 8; z++) {
				if ((z == 0.0) || (r / z >= 1.0) || (r / z <= 1.0 / 7.0)) {
					verticK = (int) (-1 * r);
				} else if ((r / z <= 1.0 / 3.0) && (r / z >= 1.0 / 5.0)) {
					verticK = (int) r;
				} else if ((r / z < 1.0) && (r / z > 1.0 / 3.0)) {
					tempb += 1;
					verticK = (int) (r * (tempb / tempa - 1));
				} else {
					tempc += 1;
					verticK = (int) (r * (1 - tempc / tempa));
				}

				if (z == 0.0 || r / z > 1.0) {
					tempd += 1;
					horizJ = (int) (r * (1 - tempd / tempa));
				} else if (r / z < 1.0 / 7.0) {
					tempf -= 1;
					horizJ = (int) (tempf / tempa);
				} else if (r / z < 1.0 / 3.0 && r / z > 1.0 / 5.0) {
					tempe += 1;
					horizJ = (int) (r * (tempe / tempa - 1));
				} else if (r / z <= 1.0 && r / z >= 1.0 / 3.0) {
					horizJ = (int) (-1 * r);
				} else {
					horizJ = (int) r;
				}

				if ((vertical + verticK >= 0 && vertical + verticK <= 15)
						&& (horizontal + horizJ >= 0 && horizontal + horizJ <= 9)) {
					if (GameField.fieldFree[vertical + verticK][horizontal
							+ horizJ]) {
						if (insertBlock(centerBlock, vertical + verticK,
								horizontal + horizJ, map)) {
							return;
						}
					}
				}
			}
		}
		state = GameState.GameOver;
		return;

	}

	private boolean insertBlock(Block centerBlock, int vertical,
			int horizontal, ArrayList<Block> map) {
		int len = map.size();
		for (int i = 0; i < len; i++) {
			if (vertical + map.get(i).vertical < 0
					|| vertical + map.get(i).vertical > 15
					|| horizontal + map.get(i).horizontal < 0
					|| horizontal + map.get(i).horizontal > 9) {
				return false;
			} else if (GameField.fieldFree[vertical + map.get(i).vertical][horizontal
					+ map.get(i).horizontal] == false) {
				return false;
			}
		}
		for (int i = 0; i < len; i++) {
			GameField.fieldFree[vertical + map.get(i).vertical][horizontal
					+ map.get(i).horizontal] = false;
			map.get(i).onTouch = false;
			map.get(i).vertical = vertical + map.get(i).vertical;
			map.get(i).horizontal = horizontal + map.get(i).horizontal;
			map.get(i).state = BlockState.OnField;
		}
		return true;
	}

	private void updatePaused(List<TouchEvent> touchEvents) {
		// Here goes the "Pause" screen, and its handling

		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type == TouchEvent.TOUCH_DOWN) {
				if (inBounds(event, Data.pauseButtX, Data.buttOffset,
						Data.buttSize, Data.buttSize)) {
					state = GameState.Running;
				}
				if (inBounds(event, Data.pauseButtX + Data.buttSize,
						Data.buttOffset, Data.buttSize, Data.buttSize)) {
					state = GameState.GameOver;
				}
			}
		}

	}

	private void updateGameOver(List<TouchEvent> touchEvents) {
		// This occurs when lose conditions are met

		if (gameOverTimer > 150) {
			if (Fingertris.data.isMusicOn() && Fingertris.data.isSoundOn())
				Assets.mainTheme.play();

			if (score < Fingertris.data.getLoScore()) {
				game.setScreen(new MainMenuScreen(game));
			} else {
				game.setScreen(new BestScoreScreen(game, score));
			}

			nullify();
			return;

		}
	}

	private void nullify() {
		// Here all objects are removed (set to null)
		gameField = null;
		left = null;
		center = null;
		right = null;
		bin = null;
		draggedListOnField.clear();
		draggedListOnField = null;
		draggedListReady.clear();
		draggedListReady = null;
		if (blockList != null) {
			for (int i = 0; i < blockList.size(); i++) {
				blockList.get(i).dispose();
			}
		}
		blockList.clear();
		blockList = null;
		pHiScore = null;
		pScore = null;

		// Call garbage collector to clear memory
		// It is only a hint to system, and GC may not be called; in rare cases
		// this may not be enough
		System.gc();

	}

	@Override
	public void paint(float deltaTime) {
		// Here game graphics are drawn
		Graphics g = game.getGraphics();

		// First draw the game elements

		// Example:
		// g.drawImage(Assets.background, 0, 0);
		// g.drawImage(Assets.character, characterX, charakterY);

		g.drawImage(Assets.gameScreen, 0, 0);

		if (state != GameState.Paused) {
			g.drawImage(Assets.pause, Data.pauseButtX, Data.buttOffset);
			g.drawImage(Assets.forfeit_ina, Data.pauseButtX + Data.buttSize,
					Data.buttOffset);
		}

		g.drawImage(Assets.next, Data.pauseButtX + 2 * Data.buttSize,
				Data.buttOffset);

		g.drawRect(Data.pauseButtX, Data.buttOffset + Data.buttSize + 15,
				(int) (Data.timeLen * timer / maxTimer), 20, 0xFFFF0000);

		if (blockList != null) {
			for (int i = 0; i < blockList.size(); i++) {
				blockList.get(i).paint(deltaTime, g);
			}
		}

		g.drawString("/", 910, 55, pHiScore);
		g.drawString(String.valueOf((long) hiScore), 1060, 55, pHiScore);

		if (score > hiScore)
			pScore.setColor(Color.RED);

		g.drawString(String.valueOf((long) score), 760, 55, pScore);

		g.drawString("Level:", 700, 340, pHiScore);
		g.drawString(String.valueOf(level + 1), 890, 340, pHiScore);

		pHiScore.setColor(Color.WHITE);
		pHiScore.setTextSize(60);
		g.drawString(String.valueOf(bin.usage), 770, 630, pHiScore);
		pHiScore.setTextSize(50);
		pHiScore.setColor(Color.BLACK);

		// Secondly, draw the UI above the game elements
		switch (state) {
		case GameOver:
			drawGameOverUI(deltaTime);
			break;
		case Paused:
			drawPausedUI();
			break;
		case Ready:
			drawReadyUI();
			break;
		case Running:
			drawRunningUI(deltaTime);
			break;
		default:
			break;

		}
	}

	private void drawReadyUI() {
		// Draws the "Ready" screen
		Graphics g = game.getGraphics();

		g.drawARGB(155, 155, 155, 155);
		pHiScore.setTextSize(80);
		g.drawString("Tap screen to start", 640, 440, pHiScore);
		pHiScore.setTextSize(50);
	}

	private void drawRunningUI(float deltaTime) {
		// Draws actual game
		Graphics g = game.getGraphics();

		gameField.paint(deltaTime, g);

	}

	private void drawPausedUI() {
		// Draws "Pause" screen
		Graphics g = game.getGraphics();

		// Darken the entire screen
		g.drawARGB(155, 155, 155, 155);
		g.drawImage(Assets.unpause, Data.pauseButtX, Data.buttOffset);
		g.drawImage(Assets.forfeit, Data.pauseButtX + Data.buttSize,
				Data.buttOffset);
	}

	private void drawGameOverUI(float deltaTime) {
		// Draws "GameOver" screen
		Graphics g = game.getGraphics();
		if (gameOverTimer == -1) {
			gameOverTimer = 1;
			if (Fingertris.data.isMusicOn() && Fingertris.data.isSoundOn())
				Assets.mainTheme.pause();

			Assets.gameOver.play(Fingertris.data.getSoundVolumeSound());
			game.showAd();
		}
		if (gameOverTimer > 0) {
			gameOverTimer += deltaTime;
		}
		if (gameOverTimer > 80 && (gameOverTimer - 80) * 4 < 255) {
			g.drawARGB((int) ((gameOverTimer - 80) * 4), 0, 0, 0);
		}
		if ((gameOverTimer - 80) * 4 > 255) {
			g.drawARGB(255, 0, 0, 0);
		}

	}

	@Override
	public void pause() {
		if (state == GameState.Running) {
			state = GameState.Paused;
		}

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		// nullify();

	}

	@Override
	public void backButton() {
		if (state == GameState.Running) {
			state = GameState.Paused;
		} else if (state == GameState.Paused) {
			state = GameState.GameOver;
		}

	}

	public static Image chooseImg() {
		Random r = new Random();
		switch (r.nextInt(13)) {
		case 0:
			return Assets.kloc;
		case 1:
			return Assets.klocA;
		case 2:
			return Assets.klocB;
		case 3:
			return Assets.klocC;
		case 4:
			return Assets.klocD;
		case 5:
			return Assets.klocE;
		case 6:
			return Assets.klocF;
		case 7:
			return Assets.klocG;
		case 8:
			return Assets.klocH;
		case 9:
			return Assets.klocI;
		case 10:
			return Assets.klocJ;
		case 11:
			return Assets.klocK;
		case 12:
			return Assets.klocL;
		default:
			return null;
		}

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub

	}

}