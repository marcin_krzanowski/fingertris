package com.mkrzanowsgames.semagswonazrkm.fingertris.framework;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics.ImageFormat;

public interface Image {
    public int getWidth();
    public int getHeight();
    public ImageFormat getFormat();
    public void dispose();
}