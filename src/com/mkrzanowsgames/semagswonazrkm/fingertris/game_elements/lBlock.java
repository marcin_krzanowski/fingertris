package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.oneBlock.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class lBlock {

	private BlockState state;

	public Block center, left, up;

	public lBlock() {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour);
		left = new Block(colour);
		up = new Block(colour);
		center.connLeft = left;
		left.connRight = center;
		center.connUp = up;
		up.connDown = center;
		state = BlockState.Ready;
	}

	public lBlock(int disp) {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp);
		left = new Block(colour, disp);
		up = new Block(colour, disp);
		center.connLeft = left;
		left.connRight = center;
		center.connUp = up;
		up.connDown = center;
		state = BlockState.Ready;
	}

	public lBlock(int disp, int x, int y) {
		int colour = GameScreen.colourList.chooseColour();
		Image img = GameScreen.chooseImg();
		center = new Block(colour, disp, x, y, true, img);
		left = new Block(colour, disp, x, y, false, img);
		up = new Block(colour, disp, x, y, false, img);
		center.connLeft = left;
		left.connRight = center;
		center.connUp = up;
		up.connDown = center;
		state = BlockState.Ready;
	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}

}
