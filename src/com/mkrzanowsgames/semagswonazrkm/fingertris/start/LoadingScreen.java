package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics.ImageFormat;

public class LoadingScreen extends Screen {
	protected final Fingertris ggame;

	public LoadingScreen(Fingertris game) {
		super(game);
		ggame = game;
	}

	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();

		if (Assets.mainMenu == null)
			Assets.mainMenu = g.newImage("mainmenu.png", ImageFormat.RGB565);

		if (Assets.levelMenu == null)
			Assets.levelMenu = g.newImage("levelmenu.png", ImageFormat.RGB565);

		if (Assets.settingsMenu == null)
			Assets.settingsMenu = g.newImage("settingsmenu.png",
					ImageFormat.RGB565);

		if (Assets.hiScoreMenu == null)
			Assets.hiScoreMenu = g.newImage("hiscoremenu.png",
					ImageFormat.RGB565);

		if (Assets.creditsMenu == null)
			Assets.creditsMenu = g.newImage("creditsmenu.png",
					ImageFormat.RGB565);

		if (Assets.check == null)
			Assets.check = g.newImage("check.png", ImageFormat.ARGB4444);

		if (Assets.checkB == null)
			Assets.checkB = g.newImage("checkB.png", ImageFormat.ARGB4444);

		if (Assets.checkC == null)
			Assets.checkC = g.newImage("checkC.png", ImageFormat.ARGB4444);
		
		if (Assets.checkD == null)
			Assets.checkD = g.newImage("checkD.png", ImageFormat.ARGB4444);

		if (Assets.lvlDown == null)
			Assets.lvlDown = game.getAudio().createSound("fgtris_lvldown.ogg");

		if (Assets.lvlUp == null)
			Assets.lvlUp = game.getAudio().createSound("fgtris_lvlup.ogg");

		if (Assets.lineFilled == null)
			Assets.lineFilled = game.getAudio().createSound(
					"fgtris_linefilled.ogg");

		if (Assets.blockRec == null)
			Assets.blockRec = game.getAudio()
					.createSound("fgtris_blockRec.ogg");

		if (Assets.blockSpwnd == null)
			Assets.blockSpwnd = game.getAudio().createSound(
					"fgtris_blockspwnd.ogg");

		if (Assets.gameOver == null)
			Assets.gameOver = game.getAudio()
					.createSound("fgtris_gameover.ogg");

		if (Assets.gameScreen == null)
			Assets.gameScreen = g
					.newImage("gamescreen.png", ImageFormat.RGB565);

		if (Assets.kloc == null)
			Assets.kloc = g.newImage("kloc.png", ImageFormat.ARGB4444);
		if (Assets.klocA == null)
			Assets.klocA = g.newImage("klocA.png", ImageFormat.ARGB4444);
		if (Assets.klocB == null)
			Assets.klocB = g.newImage("klocB.png", ImageFormat.ARGB4444);
		if (Assets.klocC == null)
			Assets.klocC = g.newImage("klocC.png", ImageFormat.ARGB4444);
		if (Assets.klocD == null)
			Assets.klocD = g.newImage("klocD.png", ImageFormat.ARGB4444);
		if (Assets.klocE == null)
			Assets.klocE = g.newImage("klocE.png", ImageFormat.ARGB4444);
		if (Assets.klocF == null)
			Assets.klocF = g.newImage("klocF.png", ImageFormat.ARGB4444);
		if (Assets.klocG == null)
			Assets.klocG = g.newImage("klocG.png", ImageFormat.ARGB4444);
		if (Assets.klocH == null)
			Assets.klocH = g.newImage("klocH.png", ImageFormat.ARGB4444);
		if (Assets.klocI == null)
			Assets.klocI = g.newImage("klocI.png", ImageFormat.ARGB4444);
		if (Assets.klocJ == null)
			Assets.klocJ = g.newImage("klocJ.png", ImageFormat.ARGB4444);
		if (Assets.klocK == null)
			Assets.klocK = g.newImage("klocK.png", ImageFormat.ARGB4444);
		if (Assets.klocL == null)
			Assets.klocL = g.newImage("klocL.png", ImageFormat.ARGB4444);

		if (Assets.forfeit == null)
			Assets.forfeit = g.newImage("forfeit.png", ImageFormat.ARGB8888);
		if (Assets.forfeit_ina == null)
			Assets.forfeit_ina = g.newImage("forfeit_ina.png",
					ImageFormat.ARGB8888);
		if (Assets.pause == null)
			Assets.pause = g.newImage("pause.png", ImageFormat.ARGB8888);
		if (Assets.unpause == null)
			Assets.unpause = g.newImage("unpause.png", ImageFormat.ARGB8888);
		if (Assets.next == null)
			Assets.next = g.newImage("next.png", ImageFormat.ARGB8888);

		if (Assets.Gi == null)
			Assets.Gi = android.graphics.Typeface.createFromAsset(
					ggame.getAssets(), "PT_Serif-Web-Italic.ttf");

		if (Assets.mainTheme == null)
			Assets.load(ggame);

		if (Assets.mainMenu != null)
			game.setScreen(new MainMenuScreen(game));
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.splash, 0, 0);

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub

	}

}
