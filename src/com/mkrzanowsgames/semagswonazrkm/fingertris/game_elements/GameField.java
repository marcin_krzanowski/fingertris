package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.Block.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.Fingertris;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class GameField {

	private int verticalFull = -1;
	private int verticalFullTwo = -1;
	private int timer = 0;
	public static boolean[][] fieldFree;

	public GameField() {
		fieldFree = new boolean[16][10];
		for (int i = 0; i < 16; i++) {
			for (int k = 0; k < 10; k++) {
				fieldFree[i][k] = true;
			}
		}

	}

	public void paint(float deltaTime, Graphics g) {
		// Draw horizontal guidelines, remove after completion
		/*
		 * g.drawLine(200, 50, 700, 50, Color.BLACK); g.drawLine(200, 100, 700,
		 * 100, Color.BLACK); g.drawLine(200, 150, 700, 150, Color.BLACK);
		 * g.drawLine(200, 200, 700, 200, Color.BLACK); g.drawLine(200, 250,
		 * 700, 250, Color.BLACK); g.drawLine(200, 300, 700, 300, Color.BLACK);
		 * g.drawLine(200, 350, 700, 350, Color.BLACK); g.drawLine(200, 400,
		 * 700, 400, Color.BLACK); g.drawLine(200, 450, 700, 450, Color.BLACK);
		 * g.drawLine(200, 500, 700, 500, Color.BLACK); g.drawLine(200, 550,
		 * 700, 550, Color.BLACK); g.drawLine(200, 600, 700, 600, Color.BLACK);
		 * g.drawLine(200, 650, 700, 650, Color.BLACK); g.drawLine(200, 700,
		 * 700, 700, Color.BLACK); g.drawLine(200, 750, 700, 750, Color.BLACK);
		 * 
		 * // Draw vertical guidelines, remove after completion g.drawLine(250,
		 * 0, 250, 800, Color.BLACK); g.drawLine(300, 0, 300, 800, Color.BLACK);
		 * g.drawLine(350, 0, 350, 800, Color.BLACK); g.drawLine(400, 0, 400,
		 * 800, Color.BLACK); g.drawLine(450, 0, 450, 800, Color.BLACK);
		 * g.drawLine(500, 0, 500, 800, Color.BLACK); g.drawLine(550, 0, 550,
		 * 800, Color.BLACK); g.drawLine(600, 0, 600, 800, Color.BLACK);
		 * g.drawLine(650, 0, 650, 800, Color.BLACK);
		 */
	}

	public void update(float deltaTime) {
		if (this.timer >= 50) {

			boolean destroy = false;
			verticalFullTwo = verticalFull;

			for (int i = 15; i >= 0; i--) {
				for (int k = 9; k >= 0; k--) {
					if (fieldFree[i][k]) {
						break;
					}
					if (k == 0) {
						verticalFull = i;
					}
				}
				if (verticalFull != -1) {
					break;
				}
			}

			if (verticalFullTwo == verticalFull && verticalFull != -1) {
				verticalFull = -1;
				destroy = true;
			}

			int len = GameScreen.blockList.size();
			for (int i = 0; i < len; i++) {
				if (GameScreen.blockList.get(i).state == BlockState.OnField) {
					if (GameScreen.blockList.get(i).isCenter
							&& GameScreen.blockList.get(i).onTouch == false) {
						if (GameScreen.blockList.get(i).checkGravity()) {
							GameScreen.blockList.get(i).pullDown();
						}
					}
					if (destroy) {
						if (GameScreen.blockList.get(i).vertical == verticalFullTwo) {

							if (GameScreen.blockList.get(i).vertical == verticalFullTwo) {
								if (GameScreen.blockList.get(i).falling
										|| GameScreen.blockList.get(i)
												.isOnTouch()) {
									destroy = false;
								}
							}
						}
					}
				}

			}
			if (destroy) {
				for (int k = 9; k >= 0; k--) {
					if (fieldFree[verticalFullTwo][k]) {
						destroy = false;
						break;

					}
				}
			}

			if (destroy) {
				for (int i = 0; i < len; i++) {
					if (GameScreen.blockList.get(i).vertical == verticalFullTwo
							&& GameScreen.blockList.get(i).state == BlockState.OnField) {
						GameScreen.blockList.get(i).state = BlockState.Destroyed;
					}
				}
				GameScreen.score += GameScreen.scoreBonus[GameScreen.level] * 10;
				Assets.lineFilled.play(Fingertris.data.getSoundVolumeSound());
			}

			this.timer = 0;
		} else {
			this.timer += deltaTime;
		}

	}

}