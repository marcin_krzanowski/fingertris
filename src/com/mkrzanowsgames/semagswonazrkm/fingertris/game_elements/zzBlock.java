package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.oneBlock.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class zzBlock {

	private BlockState state;

	public Block center, right, up, upLeft;

	public zzBlock() {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour);
		right = new Block(colour);
		up = new Block(colour);
		upLeft = new Block(colour);
		state = BlockState.Ready;
		center.connRight=right;
		center.connUp=up;
		right.connLeft=center;
		up.connDown=center;
		up.connLeft=upLeft;
		upLeft.connRight=up;
	}

	public zzBlock(int disp) {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp);
		right = new Block(colour, disp);
		up = new Block(colour, disp);
		upLeft = new Block(colour, disp);
		state = BlockState.Ready;
		center.connRight=right;
		center.connUp=up;
		right.connLeft=center;
		up.connDown=center;
		up.connLeft=upLeft;
		upLeft.connRight=up;
	}

	public zzBlock(int disp, int x, int y) {
		int colour = GameScreen.colourList.chooseColour();
		Image img = GameScreen.chooseImg();
		center = new Block(colour, disp, x, y, true, img);
		right = new Block(colour, disp, x, y, false, img);
		up = new Block(colour, disp, x, y, false, img);
		upLeft = new Block(colour, disp, x, y, false, img);
		state = BlockState.Ready;
		center.connRight=right;
		center.connUp=up;
		right.connLeft=center;
		up.connDown=center;
		up.connLeft=upLeft;
		upLeft.connRight=up;
	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}

}
