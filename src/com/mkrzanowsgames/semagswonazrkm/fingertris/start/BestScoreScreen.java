package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;
import android.view.inputmethod.InputMethodManager;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public class BestScoreScreen extends Screen {
	private Paint paint, pHScore;
	private double score;
	private String name;
	private boolean saveSuccesful;

	public BestScoreScreen(Game game) {
		super(game);
	}

	public BestScoreScreen(Game game, double score) {
		super(game);

		paint = new Paint();
		paint.setTextSize(50);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTypeface(Assets.Gi);

		pHScore = new Paint();
		pHScore.setTextSize(60);
		pHScore.setTextAlign(Paint.Align.CENTER);
		pHScore.setAntiAlias(true);
		pHScore.setColor(Color.BLACK);
		pHScore.setTypeface(Assets.Gi);

		this.score = score;
		name = Fingertris.data.getLastName();
		saveSuccesful = false;
		game.getInputMethodManager().toggleSoftInput(
				InputMethodManager.SHOW_FORCED, 0);
	}

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);

			if (event.type == TouchEvent.TOUCH_UP) {

				if (inBounds(event, 370, 225, 550, 400)) {
					game.getInputMethodManager().toggleSoftInput(
							InputMethodManager.SHOW_FORCED, 0);

				}

				if (inBounds(event, 490, 665, 300, 70)) {
					backButton();

				}
			}
		}
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.mainMenu, 0, 0);
		g.drawString("New high score!", 640, 300, pHScore);
		pHScore.setColor(Color.RED);
		g.drawString(String.valueOf((long) score), 640, 380, pHScore);
		pHScore.setColor(Color.BLACK);
		g.drawString("Congratulations", 640, 460, pHScore);
		g.drawString(name, 640, 540, pHScore);

		g.drawString("Confirm", 640, 710, paint);

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		if (!saveSuccesful) {
			Fingertris.data.setLastName(name);
			Fingertris.data.addScore(score, name);
			saveSuccesful = true;
		}
		// paint = null;
		// pHScore = null;

	}

	@Override
	public void backButton() {
		if (!saveSuccesful) {
			Fingertris.data.setLastName(name);
			Fingertris.data.addScore(score, name);
			saveSuccesful = true;
		}

		game.getInputMethodManager().hideSoftInputFromWindow(
				game.getWindowToken(), 0);

		game.setScreen(new MainMenuScreen(game));

	}

	@Override
	public void keyPressed(char c) {
		if (c == '\b') {
			if (this.name.length() > 0)
				this.name = this.name.substring(0, this.name.length() - 1);

			return;
		}

		if (c == '\n') {
			backButton();
			return;
		}

		if (this.name.length() < 10) {
			this.name = this.name.concat(String.valueOf(c));
			return;
		}

	}

}
