package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class oneBlock {

	enum BlockState {
		Ready, OnField, Destroyed, Recycled
	}

	private BlockState state;

	public Block center;

	public oneBlock() {
		center = new Block(GameScreen.colourList.chooseColour());
		state = BlockState.Ready;
	}

	public oneBlock(int disp) {
		center = new Block(GameScreen.colourList.chooseColour(), disp);
		state = BlockState.Ready;
	}

	public oneBlock(int disp, int x, int y) {
		Image img = GameScreen.chooseImg();
		center = new Block(GameScreen.colourList.chooseColour(), disp, x, y, true, img);
		state = BlockState.Ready;
	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}
}
