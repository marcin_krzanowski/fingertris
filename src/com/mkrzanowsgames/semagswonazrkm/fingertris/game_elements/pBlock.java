package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.oneBlock.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class pBlock {

	private BlockState state;

	public Block center, down, up, upRight;

	public pBlock() {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour);
		down = new Block(colour);
		up = new Block(colour);
		upRight=new Block(colour);
		center.connDown = down;
		down.connUp = center;
		center.connUp = up;
		up.connDown = center;
		up.connRight=upRight;
		upRight.connLeft=up;
		state = BlockState.Ready;
	}

	public pBlock(int disp) {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp);
		down = new Block(colour, disp);
		up = new Block(colour, disp);
		upRight=new Block(colour, disp);
		center.connDown = down;
		down.connUp = center;
		center.connUp = up;
		up.connDown = center;
		up.connRight=upRight;
		upRight.connLeft=up;
		state = BlockState.Ready;
	}

	public pBlock(int disp, int x, int y) {
		int colour = GameScreen.colourList.chooseColour();
		Image img = GameScreen.chooseImg();
		center = new Block(colour, disp, x, y, true, img);
		down = new Block(colour, disp, x, y, false, img);
		up = new Block(colour, disp, x, y, false, img);
		upRight=new Block(colour, disp, x, y, false, img);
		center.connDown = down;
		down.connUp = center;
		center.connUp = up;
		up.connDown = center;
		up.connRight=upRight;
		upRight.connLeft=up;
		state = BlockState.Ready;
	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}

}
