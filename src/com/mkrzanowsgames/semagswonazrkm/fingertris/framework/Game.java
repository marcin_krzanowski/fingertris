package com.mkrzanowsgames.semagswonazrkm.fingertris.framework;

import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;

public interface Game {

	public Audio getAudio();

	public Input getInput();
	
	public InputMethodManager getInputMethodManager();
	
	public FileIO getFileIO();

	public Graphics getGraphics();

	public void setScreen(Screen screen);

	public Screen getCurrentScreen();

	public Screen getInitScreen();

	public IBinder getWindowToken();
	
	public void hideAd();
	
	public void showAd();

}
