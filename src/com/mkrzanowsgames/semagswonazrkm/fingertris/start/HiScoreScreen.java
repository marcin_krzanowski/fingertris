package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public class HiScoreScreen extends Screen {
	private Paint paint, score, title;

	public HiScoreScreen(Game game) {
		super(game);

		paint = new Paint();
		paint.setTextSize(50);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTypeface(Assets.Gi);

		title = new Paint();
		title.setTextSize(60);
		title.setTextAlign(Paint.Align.CENTER);
		title.setAntiAlias(true);
		title.setColor(Color.BLACK);
		title.setTypeface(Assets.Gi);

		score = new Paint();
		score.setTextSize(40);
		score.setTextAlign(Paint.Align.CENTER);
		score.setAntiAlias(true);
		score.setColor(Color.BLACK);
		score.setTypeface(Assets.Gi);
	}

	@Override
	public void update(float deltaTime) {
		@SuppressWarnings("unused")
		Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		if (touchEvents != null) {
			int len = touchEvents.size();
			for (int i = 0; i < len; i++) {
				TouchEvent event = touchEvents.get(i);
				if (event.type == TouchEvent.TOUCH_UP) {
					if (inBounds(event, 490, 665, 300, 70)) {
						backButton();
					}
				}
			}
		}

	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.hiScoreMenu, 0, 0);

		g.drawString("High Score", 640, 110, title);
		g.drawString("Back", 640, 710, paint);

		for (int i = 0; i < 10; i++) {
			g.drawString(Fingertris.data.getScoreName(i), 400, 170 + 50 * i,
					score);
			g.drawString(
					String.valueOf((long) Fingertris.data.getScoreValue(i)),
					770, 170 + 50 * i, score);
		}

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		game.setScreen(new MainMenuScreen(game));

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub

	}

}
