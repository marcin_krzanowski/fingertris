package com.mkrzanowsgames.semagswonazrkm.fingertris.framework;

public interface Sound {
	public void play(float volume);

	public void dispose();
}