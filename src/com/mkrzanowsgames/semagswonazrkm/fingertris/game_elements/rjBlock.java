package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.oneBlock.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class rjBlock {

	private BlockState state;

	public Block center, left, leftDown, right, rightUp;

	public rjBlock() {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour);
		left = new Block(colour);
		right = new Block(colour);
		leftDown = new Block(colour);
		rightUp = new Block(colour);
		state = BlockState.Ready;
		center.connLeft = left;
		center.connRight = right;
		left.connRight = center;
		left.connDown = leftDown;
		leftDown.connUp = left;
		right.connLeft = center;
		right.connUp = rightUp;
		rightUp.connDown = right;
	}

	public rjBlock(int disp) {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp);
		left = new Block(colour, disp);
		right = new Block(colour, disp);
		leftDown = new Block(colour, disp);
		rightUp = new Block(colour, disp);
		center.connLeft = left;
		center.connRight = right;
		left.connRight = center;
		left.connDown = leftDown;
		leftDown.connUp = left;
		right.connLeft = center;
		right.connUp = rightUp;
		rightUp.connDown = right;
		state = BlockState.Ready;

	}

	public rjBlock(int disp, int x, int y) {
		int colour = GameScreen.colourList.chooseColour();
		Image img = GameScreen.chooseImg();
		center = new Block(colour, disp, x, y, true, img);
		left = new Block(colour, disp, x, y, false, img);
		right = new Block(colour, disp, x, y, false, img);
		leftDown = new Block(colour, disp, x, y, false, img);
		rightUp = new Block(colour, disp, x, y, false, img);
		center.connLeft = left;
		center.connRight = right;
		left.connRight = center;
		left.connDown = leftDown;
		leftDown.connUp = left;
		right.connLeft = center;
		right.connUp = rightUp;
		rightUp.connDown = right;
		state = BlockState.Ready;

	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}

}
