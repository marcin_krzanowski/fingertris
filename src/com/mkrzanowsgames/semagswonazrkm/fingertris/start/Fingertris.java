package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import android.view.KeyEvent;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Data;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.implementation.AndroidGame;

public class Fingertris extends AndroidGame {



	private boolean firstTimeCreate = true;
	public static Data data;

	@Override
	public Screen getInitScreen() {
		if (firstTimeCreate) {

			data = new Data(this);
			firstTimeCreate = false;
		}

		return new SplashScreen(this);
	}

	@Override
	public void onBackPressed() {
		getCurrentScreen().backButton();
	}

	@Override
	public void onResume() {
		super.onResume();

		if (data.isMusicOn() && Assets.mainTheme != null) {
			Assets.mainTheme.play();
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		if (data.isMusicOn()) {
			Assets.mainTheme.pause();
		}
		
		data.save();
		

	}

	@Override
	public void onDestroy() {
		if (Assets.mainTheme != null) {
			Assets.mainTheme.dispose();
		}
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int code, KeyEvent event) {
		if (code == 67) {
			getCurrentScreen().keyPressed('\b');
			return true;

		} else if ((code >= 7 && code <= 16) || (code >= 29 && code <= 54) || code == 62 || code == 66) {
			getCurrentScreen().keyPressed((char) event.getUnicodeChar());
			return true;
		}

		return super.onKeyDown(code, event);

	}

}
