package com.mkrzanowsgames.semagswonazrkm.fingertris.framework;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public abstract class Screen {
	protected final Game game;

	public Screen(Game game) {
		this.game = game;
	}

	public abstract void update(float deltaTime);

	public abstract void paint(float deltaTime);

	public abstract void pause();

	public abstract void resume();

	public abstract void dispose();

	public abstract void backButton();

	protected boolean inBounds(TouchEvent event, int x, int y, int width,
			int height) {
		if (event.x >= x && event.x < x + width && event.y >= y
				&& event.y < y + height)
			return true;
		else
			return false;
	}

	public abstract void keyPressed(char c);
}
