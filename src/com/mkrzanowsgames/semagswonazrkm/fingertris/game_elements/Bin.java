package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.Block.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.Fingertris;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class Bin {

	public int usage;
	private double tempScore;
	private int x;

	public Bin() {
		usage = 2;
		tempScore = 0;
		x = 0;
	}

	public boolean recycle(int i) {
		if (usage > 0) {
			usage -= 1;
			GameScreen.draggedListReady.get(i).state = BlockState.Recycled;
			GameScreen.draggedListReady.remove(i);
			Assets.blockRec.play(Fingertris.data.getSoundVolumeSound());
			return true;
		} else {
			return false;
		}
	}

	public void update() {
		if (GameScreen.score > tempScore * java.lang.Math.pow(2, 1 + x)
				&& usage <= 9) {
			usage += 1;
			tempScore = GameScreen.score;
			x += 1;
		} else if (GameScreen.score > tempScore * java.lang.Math.pow(2, 1 + x)) {
			tempScore = GameScreen.score;
			x += 1;
		}
	}

	public boolean recycle(Block centerBlock) {
		if (usage > 0) {
			Assets.blockRec.play(Fingertris.data.getSoundVolumeSound());
			usage -= 1;
			centerBlock.state = BlockState.Recycled;
			return true;
		} else {
			return false;
		}
	}

}
