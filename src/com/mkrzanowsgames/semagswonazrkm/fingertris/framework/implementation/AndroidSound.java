package com.mkrzanowsgames.semagswonazrkm.fingertris.framework.implementation;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Sound;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.Fingertris;

import android.media.SoundPool;

public class AndroidSound implements Sound {
	int soundId;
	SoundPool soundPool;

	public AndroidSound(SoundPool soundPool, int soundId) {
		this.soundId = soundId;
		this.soundPool = soundPool;
	}

	@Override
	public void play(float volume) {
		//TODO dep. on fingertris
		if (Fingertris.data.isSoundOn()) {
			soundPool.play(soundId, volume, volume, 0, 0, 1);
		}
	}

	@Override
	public void dispose() {
		soundPool.unload(soundId);
	}

}