package com.mkrzanowsgames.semagswonazrkm.fingertris.data;

import android.graphics.Typeface;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Music;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Sound;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.Fingertris;

public class Assets {

	public static Image settingsMenu, hiScoreMenu, levelMenu,
			creditsMenu, mainMenu, splash, check, checkB, checkC, checkD, gameScreen,
			forfeit, forfeit_ina, pause, unpause, next;
	public static Image kloc, klocA, klocB, klocC, klocD, klocE, klocF, klocG,
			klocH, klocI, klocJ, klocK, klocL;
	public static Sound lvlDown, lvlUp, lineFilled, blockRec, blockSpwnd, gameOver;
	public static Music mainTheme;
	public static Typeface Gi;

	public static void load(Fingertris game) {
		mainTheme = game.getAudio().createMusic("ftris_theme_main.ogg");
		mainTheme.setLooping(true);
		mainTheme
				.setVolume((float) com.mkrzanowsgames.semagswonazrkm.fingertris.start.Fingertris.data
						.getSoundVolumeMusic());

	}
}