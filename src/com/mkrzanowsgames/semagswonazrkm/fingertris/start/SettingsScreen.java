package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public class SettingsScreen extends Screen {
	int tchPointer = -1;
	int tchPointer2 = -1;
	private Paint paint, title;

	public SettingsScreen(Game game) {
		super(game);

		paint = new Paint();
		paint.setTextSize(50);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTypeface(Assets.Gi);

		title = new Paint();
		title.setTextSize(60);
		title.setTextAlign(Paint.Align.CENTER);
		title.setAntiAlias(true);
		title.setColor(Color.BLACK);
		title.setTypeface(Assets.Gi);

	}

	@Override
	public void update(float deltaTime) {
		@SuppressWarnings("unused")
		Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		if (touchEvents != null) {
			int len = touchEvents.size();
			for (int i = 0; i < len; i++) {
				TouchEvent event = touchEvents.get(i);
				if (event.type == TouchEvent.TOUCH_UP) {
					if (tchPointer == event.pointer) {
						tchPointer = -1;
						continue;
					}
					if (tchPointer2 == event.pointer) {
						tchPointer2 = -1;
						Assets.lvlUp
								.play(Fingertris.data.getSoundVolumeSound());
						continue;
					}

					if (inBounds(event, 290, 365, 425, 70)) {
						if (Fingertris.data.isMusicOn()) {
							Fingertris.data.setMusicOn(false);
							Assets.mainTheme.pause();
						} else {
							Fingertris.data.setMusicOn(true);
							Assets.mainTheme.play();
						}
					}

					if (inBounds(event, 290, 185, 425, 70)) {
						Fingertris.data
								.setSoundOn(!Fingertris.data.isSoundOn());
						Assets.lvlUp
								.play(Fingertris.data.getSoundVolumeSound());
					}

					if (inBounds(event, 490, 570, 300, 70)) {
						game.setScreen(new CreditsScreen(game));
						return;
					}

					if (inBounds(event, 490, 665, 300, 70)) {
						backButton();
						return;
					}
				}
				if (event.type == TouchEvent.TOUCH_DOWN) {
					if (inBounds(event, 290, 440, 700, 70)) {
						if (tchPointer == -1) {
							tchPointer = event.pointer;
							setVolMusic(event.x);
						}
					}
					if (inBounds(event, 290, 260, 700, 70)) {
						if (tchPointer2 == -1) {
							tchPointer2 = event.pointer;
							setVolSound(event.x);
						}
					}
				}
				if (event.type == TouchEvent.TOUCH_DRAGGED) {
					if (tchPointer == event.pointer) {
						setVolMusic(event.x);
					}
					if (tchPointer2 == event.pointer) {
						setVolSound(event.x);

					}
				}
			}
		}
	}

	private void setVolMusic(int x) {
		if (x < 290) {
			Fingertris.data.setSoundVolumeMusic(0);
		} else if (x > 920) {
			Fingertris.data.setSoundVolumeMusic(1);
		} else {
			Fingertris.data.setSoundVolumeMusic((float) ((x - 290) / 650.0));
		}
		Assets.mainTheme.setVolume(Fingertris.data.getSoundVolumeMusic());
	}

	private void setVolSound(int x) {
		if (x < 290) {
			Fingertris.data.setSoundVolumeSound(0);
		} else if (x > 920) {
			Fingertris.data.setSoundVolumeSound(1);
		} else {
			Fingertris.data.setSoundVolumeSound((float) ((x - 290) / 650.0));
		}
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.settingsMenu, 0, 0);
		if (Fingertris.data.isMusicOn())
			g.drawImage(Assets.check, 650, 360);

		if (Fingertris.data.isSoundOn())
			g.drawImage(Assets.checkB, 650, 180);

		g.drawImage(Assets.checkD,
				280 + (int) (650 * Fingertris.data.getSoundVolumeSound()), 260);

		g.drawImage(Assets.checkC,
				280 + (int) (650 * Fingertris.data.getSoundVolumeMusic()), 440);

		g.drawString("Settings", 640, 120, title);

		g.drawString("Sound", 460, 230, paint);

		g.drawString("Music", 460, 410, paint);

		g.drawString("Credits", 640, 610, paint);
		g.drawString("Back", 640, 710, paint);

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		game.setScreen(new MainMenuScreen(game));

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub

	}

}
