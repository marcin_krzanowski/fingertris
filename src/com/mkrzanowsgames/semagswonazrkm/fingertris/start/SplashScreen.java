package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics.ImageFormat;

public class SplashScreen extends Screen {
	protected final Fingertris ggame;

	public SplashScreen(Fingertris game) {
		super(game);
		ggame = game;
	}

	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
		
		if (Assets.splash==null){
			Assets.splash = g.newImage("splash.png", ImageFormat.RGB565);
		} else{
			game.setScreen(new LoadingScreen(ggame));
		}

	}

	@Override
	public void paint(float deltaTime) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void backButton() {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub
		
	}

}
