package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.oneBlock.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class iBlock {

	private BlockState state;

	public Block center, left, right;

	public iBlock() {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour);
		left = new Block(colour);
		right = new Block(colour);
		center.connLeft = left;
		left.connRight = center;
		center.connRight = right;
		right.connLeft = center;
		state = BlockState.Ready;
	}

	public iBlock(int disp) {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp);
		left = new Block(colour, disp);
		right = new Block(colour,disp);
		center.connLeft = left;
		left.connRight = center;
		center.connRight = right;
		right.connLeft = center;
		state = BlockState.Ready;
	}

	public iBlock(int disp, int x, int y) {
		int colour = GameScreen.colourList.chooseColour();
		Image img = GameScreen.chooseImg();
		center = new Block(colour, disp, x, y, true, img);
		left = new Block(colour, disp, x, y, false, img);
		right = new Block(colour, disp, x, y, false, img);
		center.connLeft = left;
		left.connRight = center;
		center.connRight = right;
		right.connLeft = center;
		state = BlockState.Ready;
	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}

}
