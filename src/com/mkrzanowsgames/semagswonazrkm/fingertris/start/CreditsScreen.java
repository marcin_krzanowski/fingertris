package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public class CreditsScreen extends Screen {
	private Paint paint;
	
	public CreditsScreen(Game game) {
		super(game);

		paint = new Paint();
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTypeface(Assets.Gi);
		
	}

	@Override
	public void update(float deltaTime) {
		@SuppressWarnings("unused")
		Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		int len = touchEvents.size();
		if (len > 0) {
			for (int i = 0; i < len; i++) {
				TouchEvent event = touchEvents.get(i);
				if (event.type == TouchEvent.TOUCH_UP) {
					if (inBounds(event, 490, 665, 300, 70)) {
						backButton();

					}
				}
			}
		}

	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.creditsMenu, 0, 0);
		paint.setTextSize(32);
		g.drawString("Game made by Marcin Krzanowski", 640, 230, paint);
		g.drawString("You can find me on Twitter @marine_776", 640, 270, paint);
		
		g.drawString("Music based on works of J. S. Bach. Original sheets", 640, 328, paint);
		g.drawString("licensed under Public Domain from http://imslp.org/.", 640, 368, paint);
		
		g.drawString("Font copyright 2010, ParaType Ltd.", 640, 430, paint);
		g.drawString("(http://www.paratype.com/public), with Reserved Font Names", 640, 470, paint);
		g.drawString("'PT Sans', 'PT Serif' and 'ParaType'.This Font Software is ", 640, 510, paint);
		g.drawString("licensed under the SIL Open Font License, Version 1.1.", 640, 550, paint);
		
		g.drawString("Thank You for playing!", 640, 620, paint);
		paint.setTextSize(50);
		g.drawString("Back", 640, 710, paint);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		game.setScreen(new SettingsScreen(game));

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub
		
	}

}
