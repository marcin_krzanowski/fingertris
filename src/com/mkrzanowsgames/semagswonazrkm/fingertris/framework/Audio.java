package com.mkrzanowsgames.semagswonazrkm.fingertris.framework;

public interface Audio {
    public Music createMusic(String file);

    public Sound createSound(String file);
}