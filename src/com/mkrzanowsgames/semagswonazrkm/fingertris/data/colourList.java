package com.mkrzanowsgames.semagswonazrkm.fingertris.data;

import java.util.Random;

public class colourList {

	private int[] colours;
	private boolean[] used;

	public colourList() {
		colours = new int[180];
		used = new boolean[180];
		java.util.Arrays.fill(used, false);

		colours[0] = 0xFFB0171F;
		colours[1] = 0xFFDC143C;
		colours[2] = 0xFFFFB6C1;
		colours[3] = 0xFFCD8C95;
		
		colours[4] = 0xFFFFC0CB;
		
		colours[5] = 0xFFEEA9B8;
		
		colours[6] = 0xFFDB7093;
		
		colours[7] = 0xFFFF82AB;
		
		colours[8] = 0xFF8B475D;
		
		colours[9] = 0xFFFF3E96;
		
		colours[10] = 0xFFCD3278;
		
		colours[11] = 0xFFFF69B4;
		
		colours[12] = 0xFFCD6090;
		
		colours[13] = 0xFF872657;
		
		colours[14] = 0xFFEE1289;
		
		colours[15] = 0xFFFF34B3;
		
		colours[16] = 0xFFC71585;
		
		colours[17] = 0xFFDA70D6;
		
		colours[18] = 0xFFFF83FA;
		
		colours[19] = 0xFFCD69C9;
		
		colours[20] = 0xFF8B4789;
		
		colours[21] = 0xFFFFBBFF;
		colours[22] = 0xFFCD96CD;
		
		colours[23] = 0xFFFF00FF;
		
		colours[24] = 0xFFCD00CD;
		
		colours[25] = 0xFF800080;
		
		colours[26] = 0xFFBA55D3;
		
		colours[27] = 0xFFE066FF;
		
		colours[28] = 0xFF7A378B;
		
		colours[29] = 0xFF9400D3;
		
		colours[30] = 0xFFBF3EFF;
		
		colours[31] = 0xFF4B0082;
		
		colours[32] = 0xFF8A2BE2;
		
		colours[33] = 0xFF912CEE;
		
		colours[34] = 0xFFAB82FF;
		
		colours[35] = 0xFF8968CD;
		
		colours[36] = 0xFF483D8B;
		
		colours[37] = 0xFF8470FF;
		
		colours[38] = 0xFF7B68EE;
		
		colours[39] = 0xFF6A5ACD;
		
		colours[40] = 0xFFE6E6FA;
		
		colours[41] = 0xFF0000FF;
		
		colours[42] = 0xFF0000CD;
		
		colours[43] = 0xFF00008B;
		
		colours[44] = 0xFF000080;
		
		colours[45] = 0xFF3D59AB;
		
		colours[46] = 0xFF4169E1;
		
		colours[47] = 0xFF6495ED;
		
		colours[48] = 0xFFB0C4DE;
		
		colours[49] = 0xFFCAE1FF;
		
		colours[50] = 0xFFA2B5CD;
		
		colours[51] = 0xFF708090;
		
		colours[52] = 0xFF9FB6CD;
		
		colours[53] = 0xFF1E90FF;
		
		colours[54] = 0xFF4682B4;
		
		colours[55] = 0xFF63B8FF;
		
		colours[56] = 0xFF87CEFA;
		
		colours[57] = 0xFF8DB6CD;
		
		colours[58] = 0xFF87CEFF;
		
		colours[59] = 0xFF6CA6CD;
		
		colours[60] = 0xFF87CEEB;
		
		colours[61] = 0xFF00BFFF;
		
		colours[62] = 0xFF33A1C9;
		
		colours[63] = 0xFF9AC0CD;
		
		colours[64] = 0xFFB0E0E6;
		
		colours[65] = 0xFF8EE5EE;
		
		colours[66] = 0xFF00F5FF;
		
		colours[67] = 0xFF00E5EE;
		
		colours[68] = 0xFF00868B;
		
		colours[69] = 0xFF5F9EA0;
		
		colours[70] = 0xFFB4CDCD;
		
		colours[71] = 0xFF96CDCD;
		
		colours[72] = 0xFF2F4F4F;
		
		colours[73] = 0xFF79CDCD;
		
		colours[74] = 0xFF00EEEE;
		
		colours[75] = 0xFF008080;
		
		colours[76] = 0xFF48D1CC;
		
		colours[77] = 0xFF03A89E;
		
		colours[78] = 0xFF00C78C;
		
		colours[79] = 0xFF76EEC6;
		
		colours[80] = 0xFF00FA9A;
		
		colours[81] = 0xFF00FF7F;
		
		colours[82] = 0xFF00CD66;
		
		colours[83] = 0xFF008B45;
		
		colours[84] = 0xFF3CB371;
		
		colours[85] = 0xFF54FF9F;
		
		colours[86] = 0xFF4EEE94;
		
		colours[87] = 0xFF43CD80;
		
		colours[88] = 0xFF2E8B57;
		
		colours[89] = 0xFF00C957;
		
		colours[90] = 0xFF3D9140;
		
		colours[91] = 0xFF8FBC8F;
		
		colours[92] = 0xFF98FB98;
		
		colours[93] = 0xFF7CCD7C;
		
		colours[94] = 0xFF548B54;
		
		colours[95] = 0xFF32CD32;
		
		colours[96] = 0xFF228B22;
		
		colours[97] = 0xFF00FF00;
		
		colours[98] = 0xFF00EE00;
		
		colours[99] = 0xFF00CD00;
		
		colours[100] = 0xFF008B00;
		
		colours[101] = 0xFF006400;
		
		colours[102] = 0xFF308014;
		
		colours[103] = 0xFF7CFC00;
		
		colours[104] = 0xFF76EE00;
		
		colours[105] = 0xFF66CD00;
		
		colours[106] = 0xFF458B00;
		
		colours[107] = 0xFFADFF2F;
		
		colours[108] = 0xFFCAFF70;
		
		colours[109] = 0xFFBCEE68;
		
		colours[110] = 0xFFA2CD5A;
		
		colours[111] = 0xFF556B2F;
		
		colours[112] = 0xFF6B8E23;
		
		colours[113] = 0xFFC0FF3E;
		
		colours[114] = 0xFFB3EE3A;
		
		colours[115] = 0xFF698B22;
		
		colours[116] = 0xFFFFFF00;
		
		colours[117] = 0xFFEEEE00;
		
		colours[118] = 0xFFCDCD00;
		
		colours[119] = 0xFF8B8B00;
		
		colours[120] = 0xFFBDB76B;
		
		colours[121] = 0xFFFFF68F;
		
		colours[122] = 0xFFCDC673;
		
		colours[123] = 0xFFEEE8AA;
		
		colours[124] = 0xFFFFEC8B;
		
		colours[125] = 0xFFCDBE70;
		
		colours[126] = 0xFFE3CF57;
		
		colours[127] = 0xFFFFD700;
		
		colours[128] = 0xFFEEC900;
		
		colours[129] = 0xFFCDAD00;
		
		colours[130] = 0xFFDAA520;
		
		colours[131] = 0xFFFFC125;
		
		colours[132] = 0xFFCD9B1D;
		
		colours[133] = 0xFFB8860B;
		
		colours[134] = 0xFFFFB90F;
		
		colours[135] = 0xFFFFA500;
		
		colours[136] = 0xFFEE9A00;
		
		colours[137] = 0xFFCD8500;
		
		colours[138] = 0xFFF5DEB3;
		
		colours[139] = 0xFFD2B48C;
		
		colours[140] = 0xFF9C661F;
		
		colours[141] = 0xFFFF9912;
		
		colours[142] = 0xFFDEB887;
		
		colours[143] = 0xFFCDAA7D;
		
		colours[144] = 0xFFE3A869;
		
		colours[145] = 0xFFED9121;
		
		colours[146] = 0xFFFF7F00;
		
		colours[147] = 0xFFFF8000;
		
		colours[148] = 0xFFEE9A49;
		
		colours[149] = 0xFFF4A460;
		
		colours[150] = 0xFFC76114;
		
		colours[151] = 0xFFD2691E;
		
		colours[152] = 0xFFCD661D;
		
		colours[153] = 0xFF8B4513;
		
		colours[154] = 0xFFFF7D40;
		
		colours[155] = 0xFFFF6103;
		
		colours[156] = 0xFF8A360F;
		
		colours[157] = 0xFFFF8247;
		
		colours[158] = 0xFFFFA07A;
		
		colours[159] = 0xFFEE9572;
		
		colours[160] = 0xFFFF7F50;
		
		colours[161] = 0xFFFF4500;
		
		colours[162] = 0xFFCD3700;
		
		colours[163] = 0xFF8B2500;
		
		colours[164] = 0xFF5E2612;
		
		colours[165] = 0xFF8A3324;
		
		colours[166] = 0xFFFF6347;
		
		colours[167] = 0xFFCD4F39;
		
		colours[168] = 0xFFEEB4B4;
		
		colours[169] = 0xFFCD5C5C;
		
		colours[170] = 0xFFA52A2A;
		
		colours[171] = 0xFFFF3030;
		
		colours[172] = 0xFF8B1A1A;
		
		colours[173] = 0xFF8E388E;
		
		colours[174] = 0xFF7D9EC0;
		
		colours[175] = 0xFF388E8E;
		
		colours[176] = 0xFF71C671;
		
		colours[177] = 0xFFCD5B45;
		
		colours[178] = 0xFFE9967A;
		
		colours[179] = 0xFFFF6A6A;

	}

	private boolean isColorUsed(int num) {
		return used[num];
	}

	private int useColor(int num) {
		used[num] = true;
		return colours[num];
	}

	public int chooseColour() {
		Random r = new Random();
		int num;

		num = r.nextInt(180);
		for (int i = 0;; i++) {
			if (isColorUsed(num + i) == false) {
				return useColor(num + i);
			}
			if (num + i == 179) {
				num = 0;
				i = 0;
			}
		}
	}

	public void freeColour(int colour) {
		for (int i=0;i<180;i++){
			if (colours[i]==colour){
				used[i]=false;
				return;
			}
		}
	}

}
