package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.oneBlock.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class twoBlock {

	private BlockState state;

	public Block center, left;

	public twoBlock() {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour);
		left = new Block(colour);
		center.connLeft = left;
		left.connRight = center;
		state = BlockState.Ready;
	}

	public twoBlock(int disp) {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp);
		left = new Block(colour, disp);
		center.connLeft = left;
		left.connRight = center;
		state = BlockState.Ready;
	}

	public twoBlock(int disp, int x, int y) {
		Image img = GameScreen.chooseImg();
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp, x, y, true, img);
		left = new Block(colour, disp, x, y, false, img);
		center.connLeft = left;
		left.connRight = center;
		state = BlockState.Ready;
	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}

}
