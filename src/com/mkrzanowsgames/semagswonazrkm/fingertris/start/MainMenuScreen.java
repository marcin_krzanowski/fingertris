package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public class MainMenuScreen extends Screen {

	private Paint paint;
	private boolean once=false;

	public MainMenuScreen(Game game) {
		super(game);
		
		paint = new Paint();
		paint.setTextSize(50);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTypeface(Assets.Gi);
		

	}

	@Override
	public void update(float deltaTime) {
		@SuppressWarnings("unused")
		Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		int len = touchEvents.size();
		if (len > 0) {
			for (int i = 0; i < len; i++) {
				TouchEvent event = touchEvents.get(i);

				if (event.type == TouchEvent.TOUCH_UP) {
					if (inBounds(event, 490, 265, 300, 70)) {
						game.setScreen(new LevelMenuScreen(game));
						return;

					}
					if (inBounds(event, 490, 365, 300, 70)) {
						game.setScreen(new HiScoreScreen(game));
						return;

					}
					if (inBounds(event, 490, 465, 300, 70)) {
						game.setScreen(new SettingsScreen(game));
						return;

					}
					if (inBounds(event, 490, 565, 300, 70)) {
						game.setScreen(new InstructionsScreen(game));
						return;

					}
					if (inBounds(event, 490, 665, 300, 70)) {
						backButton();

					}
				}
			}
		}
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.mainMenu, 0, 0);
		
		g.drawString("New Game", 640, 310, paint);
		g.drawString("High Score", 640, 410, paint);
		g.drawString("Settings", 640, 510, paint);
		g.drawString("Instructions", 640, 610, paint);
		g.drawString("Exit", 640, 710, paint);
		
		if (!once && Fingertris.data.isMusicOn() && !Assets.mainTheme.isPlaying()) {
			once=true;
			Assets.mainTheme.play();
		}

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		Fingertris.data.save();
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub
		
	}

}
