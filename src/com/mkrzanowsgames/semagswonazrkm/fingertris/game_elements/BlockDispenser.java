package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import java.util.Random;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Data;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.Fingertris;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class BlockDispenser {

	public boolean touched = false;
	private oneBlock oneBlock;
	private twoBlock twoBlock;
	private lBlock lBlock;
	private iBlock iBlock;
	private zBlock zBlock;
	private pBlock pBlock;
	private rpBlock rpBlock;
	private tBlock tBlock;
	private ttBlock ttBlock;
	private xBlock xBlock;
	private zzBlock zzBlock;
	private uBlock uBlock;
	private jBlock jBlock;
	private rjBlock rjBlock;

	public boolean ready;
	private int number;
	public int x, y;
	public Block center;

	public BlockDispenser() {
		ready = false;
		number = -1;
		coordinates();
	}

	public BlockDispenser(boolean ready, int number) {
		this.ready = ready;
		this.number = number;
		coordinates();
	}

	public boolean update(float deltaTime, boolean now, boolean suppress) {

		if (center != null) {
			center.x = this.x;
			center.y = this.y;
		}

		if (ready && now) {
			center = generateBlock(suppress);

			ready = false;
			return true;
		} else if (ready == false && now) {
			return false;
		}
		return true;
	}

	public Block generateBlock(boolean suppress) {
		if (!suppress) {
			Assets.blockSpwnd.play(Fingertris.data.getSoundVolumeSound());
		}
		
		Block lCenter;

		Random r = new Random();
		int s = 26;
		if (Fingertris.data.getLastName() == "bUg")
			s += 2;

		switch (r.nextInt(s)) {
		case 0:
			oneBlock = new oneBlock(number, x, y);
			GameScreen.blockList.add(oneBlock.center);
			lCenter = oneBlock.center;
			break;
		
		case 14:
		case 1:
			twoBlock = new twoBlock(number, x, y);
			GameScreen.blockList.add(twoBlock.center);
			GameScreen.blockList.add(twoBlock.left);
			lCenter =  twoBlock.center;
			break;

		case 15:
		case 16:
		case 2:
			lBlock = new lBlock(number, x, y);
			GameScreen.blockList.add(lBlock.center);
			GameScreen.blockList.add(lBlock.left);
			GameScreen.blockList.add(lBlock.up);
			lCenter =  lBlock.center;
			break;
			
		case 13:
		case 17:
		case 3:
			iBlock = new iBlock(number, x, y);
			GameScreen.blockList.add(iBlock.center);
			GameScreen.blockList.add(iBlock.left);
			GameScreen.blockList.add(iBlock.right);
			lCenter =  iBlock.center;
			break;
			
		case 18:
		case 4:
			zBlock = new zBlock(number, x, y);
			GameScreen.blockList.add(zBlock.center);
			GameScreen.blockList.add(zBlock.left);
			GameScreen.blockList.add(zBlock.up);
			GameScreen.blockList.add(zBlock.upRight);
			lCenter =  zBlock.center;
			break;
			
		case 20:
		case 5:
			pBlock = new pBlock(number, x, y);
			GameScreen.blockList.add(pBlock.center);
			GameScreen.blockList.add(pBlock.down);
			GameScreen.blockList.add(pBlock.up);
			GameScreen.blockList.add(pBlock.upRight);
			lCenter =  pBlock.center;
			break;
			
		case 21:
		case 6:
			rpBlock = new rpBlock(number, x, y);
			GameScreen.blockList.add(rpBlock.center);
			GameScreen.blockList.add(rpBlock.down);
			GameScreen.blockList.add(rpBlock.up);
			GameScreen.blockList.add(rpBlock.upLeft);
			lCenter =  rpBlock.center;
			break;
			
		case 22:
		case 23:
		case 7:
			tBlock = new tBlock(number, x, y);
			GameScreen.blockList.add(tBlock.center);
			GameScreen.blockList.add(tBlock.right);
			GameScreen.blockList.add(tBlock.up);
			GameScreen.blockList.add(tBlock.left);
			lCenter =  tBlock.center;
			break;
			
		case 24:
		case 8:
			xBlock = new xBlock(number, x, y);
			GameScreen.blockList.add(xBlock.center);
			GameScreen.blockList.add(xBlock.down);
			GameScreen.blockList.add(xBlock.up);
			GameScreen.blockList.add(xBlock.right);
			GameScreen.blockList.add(xBlock.left);
			lCenter =  xBlock.center;
			break;
			
		case 25:
		case 9:
			zzBlock = new zzBlock(number, x, y);
			GameScreen.blockList.add(zzBlock.center);
			GameScreen.blockList.add(zzBlock.right);
			GameScreen.blockList.add(zzBlock.up);
			GameScreen.blockList.add(zzBlock.upLeft);
			lCenter =  zzBlock.center;
			break;
			
		case 19:
		case 10:
			ttBlock = new ttBlock(number, x, y);
			GameScreen.blockList.add(ttBlock.center);
			GameScreen.blockList.add(ttBlock.down);
			GameScreen.blockList.add(ttBlock.up);
			GameScreen.blockList.add(ttBlock.upRight);
			GameScreen.blockList.add(ttBlock.upLeft);
			lCenter =  ttBlock.center;
			break;
			
		case 11:
			jBlock = new jBlock(number, x, y);
			GameScreen.blockList.add(jBlock.center);
			GameScreen.blockList.add(jBlock.left);
			GameScreen.blockList.add(jBlock.right);
			GameScreen.blockList.add(jBlock.leftUp);
			GameScreen.blockList.add(jBlock.rightDown);
			lCenter =  jBlock.center;
			break;
			
		case 12:
			rjBlock = new rjBlock(number, x, y);
			GameScreen.blockList.add(rjBlock.center);
			GameScreen.blockList.add(rjBlock.left);
			GameScreen.blockList.add(rjBlock.right);
			GameScreen.blockList.add(rjBlock.leftDown);
			GameScreen.blockList.add(rjBlock.rightUp);
			lCenter =  rjBlock.center;
			break;
			
		default:
			// TODO broken?...
			uBlock = new uBlock(number, x, y);
			GameScreen.blockList.add(uBlock.center);
			GameScreen.blockList.add(uBlock.left);
			GameScreen.blockList.add(uBlock.right);
			GameScreen.blockList.add(uBlock.leftUp);
			GameScreen.blockList.add(uBlock.rightUp);
			lCenter =  uBlock.center;
		}
		
		for (int i = 0; i < r.nextInt(5); i++) {
			lCenter.flip();
		}
		
		return lCenter;

	}

	public void coordinates() {
		this.y = Data.dispOffset + Data.buttSize / 2;
		switch (number) {
		case 1:
			this.x = Data.pauseButtX + Data.buttSize / 2;
			break;
		case 2:
			this.x = Data.pauseButtX + Data.buttSize / 2 + Data.buttSize;
			break;
		case 3:
			this.x = Data.pauseButtX + Data.buttSize / 2 + Data.buttSize * 2;
			break;
		default:
			this.x = 10000;
			break;
		}
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
