package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import java.util.ArrayList;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Data;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class Block {

	public enum BlockState {
		Ready, OnField, Destroyed, Recycled
	}

	private Image img;

	public boolean onTouch = false;
	public boolean isCenter = false;
	public boolean free = true;
	public boolean falling = false;
	private boolean onceDestroyed = false;
	public BlockState state;
	public int vertical;
	public int horizontal;
	public int colour;
	public int touchPointer = -1;
	public Block connLeft, connRight, connUp, connDown;
	public int x, y;
	public int timer = 0;

	public Block() {
		vertical = -1;
		horizontal = -1;
		colour = 0xFF000000;
		state = BlockState.Ready;
		connLeft = connRight = connUp = connDown = null;
		x = y = 0;
	}

	public Block(int colour) {
		vertical = -1;
		horizontal = -1;
		this.colour = colour;
		state = BlockState.Ready;
		connLeft = connRight = connUp = connDown = null;
		this.x = this.y = 0;
	}

	public Block(int colour, int disp) {
		this.vertical = -1;
		this.horizontal = -disp;
		this.colour = colour;
		this.state = BlockState.Ready;
		this.connLeft = this.connRight = this.connUp = this.connDown = null;
		this.x = this.y = 0;
	}

	public Block(int colour, int vertical, int horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
		this.colour = colour;
		this.state = BlockState.Ready;
		this.connLeft = this.connRight = this.connUp = this.connDown = null;
		this.x = this.y = 0;
	}

	public Block(int colour, int disp, int x, int y) {
		this.vertical = -1;
		this.horizontal = -disp;
		this.colour = colour;
		this.state = BlockState.Ready;
		this.connLeft = this.connRight = this.connUp = this.connDown = null;
		this.x = x;
		this.y = y;
	}

	public Block(int colour, int disp, int x, int y, boolean isCenter) {
		this.vertical = -1;
		this.horizontal = -disp;
		this.colour = colour;
		this.state = BlockState.Ready;
		this.connLeft = this.connRight = this.connUp = this.connDown = null;
		this.x = x;
		this.y = y;
		this.isCenter = isCenter;
	}

	public Block(int colour, int disp, int x, int y, boolean isCenter, Image img) {
		this.vertical = -1;
		this.horizontal = -disp;
		this.colour = colour;
		this.state = BlockState.Ready;
		this.connLeft = this.connRight = this.connUp = this.connDown = null;
		this.x = x;
		this.y = y;
		this.isCenter = isCenter;
		this.img = img;
	}

	public void paint(float deltaTime, Graphics g) {
		switch (state) {
		case Destroyed:
			if (timer >= 0 && timer <= 15) {
				g.drawRect(x - Data.blockSize2/2, y - Data.blockSize2/2, Data.blockSize2, Data.blockSize2, colour);
				g.drawImage(img, x - Data.blockSize2/2, y - Data.blockSize2/2);
			} else if (timer > 15 && timer <= 25) {
				g.drawRect(x - Data.blockSize2/2, y - Data.blockSize2/2, Data.blockSize2, Data.blockSize2, 0xFFFFFFF);
				g.drawImage(img, x - Data.blockSize2/2, y - Data.blockSize2/2);
			} else if (timer > 25 && timer <= 40) {
				g.drawRect(x - Data.blockSize2/2, y - Data.blockSize2/2, Data.blockSize2, Data.blockSize2, colour);
				g.drawImage(img, x - Data.blockSize2/2, y - Data.blockSize2/2);
			} else if (timer > 40 && timer <= 50) {
				g.drawRect(x - Data.blockSize2/2, y - Data.blockSize2/2, Data.blockSize2, Data.blockSize2, 0xFFFFFFF);
				g.drawImage(img, x - Data.blockSize2/2, y - Data.blockSize2/2);
			} else {
			}
			break;
		case OnField:
			g.drawRect(x - Data.blockSize2/2, y - Data.blockSize2/2, Data.blockSize2, Data.blockSize2, colour);
			g.drawImage(img, x - Data.blockSize2/2, y - Data.blockSize2/2);
			break;
		case Ready:
			// TODO scaling while in disposer
			g.drawRect(x - Data.blockSize2/2, y - Data.blockSize2/2, Data.blockSize2, Data.blockSize2, colour);
			g.drawImage(img, x - Data.blockSize2/2, y - Data.blockSize2/2);
			break;
		case Recycled:
			break;
		default:
			break;

		}
	}

	public void update(float deltaTime) {
		switch (state) {
		case Destroyed:

			if (onceDestroyed == false) {
				if (connLeft != null) {
					if (isCenter) {
						connLeft.isCenter = true;
					}

					connLeft.state = BlockState.Destroyed;
					connLeft.connRight = null;
				}
				if (connRight != null) {
					if (isCenter) {
						connRight.isCenter = true;
					}

					connRight.state = BlockState.Destroyed;
					connRight.connLeft = null;
				}
				if (connDown != null) {
					if (isCenter) {
						connDown.isCenter = true;
					}

					connDown.connUp = null;
				}
				if (connUp != null) {
					if (isCenter) {
						connUp.isCenter = true;
					}

					connUp.connDown = null;
				}
				onceDestroyed = true;
			}

			timer += deltaTime;
			if (timer > 50) {
				GameField.fieldFree[vertical][horizontal] = true;
				for (int i = 0; i < GameScreen.blockList.size(); i++) {
					if (GameScreen.blockList.get(i) == this) {
						GameScreen.blockList.remove(i);

						break;
					}

				}
				for (int i = 0; i < GameScreen.blockList.size(); i++) {
					if (GameScreen.blockList.get(i).colour == colour) {
						return;
					}
				}
				GameScreen.colourList.freeColour(colour);
				this.dispose();
			}

			break;
		case OnField:
			if (isCenter) {
				this.x = Data.gameFieldOffset+Data.blockSize2/2 + this.horizontal * Data.blockSize2;
				this.y = Data.blockSize2/2 + this.vertical * Data.blockSize2;

				ArrayList<Block> mapa = map(new ArrayList<Block>(), vertical,
						horizontal);
				for (int i = 1; i < mapa.size(); i++) {

					mapa.get(i).x = this.x
							- (horizontal - mapa.get(i).horizontal) * Data.blockSize2;
					mapa.get(i).y = this.y - (vertical - mapa.get(i).vertical)
							* Data.blockSize2;
				}
			}
			break;
		case Ready:
			if (isCenter) {
				ArrayList<Block> mapa = map(new ArrayList<Block>(), vertical,
						horizontal);
				for (int i = 1; i < mapa.size(); i++) {
					mapa.get(i).x = this.x
							- (horizontal - mapa.get(i).horizontal) * Data.blockSize2;
					mapa.get(i).y = this.y - (vertical - mapa.get(i).vertical)
							* Data.blockSize2;
				}
			}
			break;
		case Recycled:
			ArrayList<Block> mapa = map(new ArrayList<Block>(), vertical,
					horizontal);
			for (int i = 1; i < mapa.size(); i++) {
				mapa.get(i).state = BlockState.Recycled;
			}
			if (connLeft != null) {
				connLeft.connRight = null;
			}
			if (connRight != null) {
				connRight.connLeft = null;
			}
			if (connDown != null) {
				connDown.connUp = null;
			}
			if (connUp != null) {
				connUp.connDown = null;
			}
			for (int i = 0; i < GameScreen.blockList.size(); i++) {
				if (GameScreen.blockList.get(i) == this) {
					GameScreen.blockList.remove(i);
					break;
				}
			}
			
			GameScreen.colourList.freeColour(colour);
			this.dispose();

			break;
		default:
			break;

		}
	}

	public ArrayList<Block> map(ArrayList<Block> mapa, int vertical,
			int horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
		mapa.add(this);

		if (this.connDown != null) {
			mapa = connDown.mapD(mapa, vertical + 1, horizontal);
		}
		if (this.connLeft != null) {
			mapa = connLeft.mapL(mapa, vertical, horizontal - 1);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR(mapa, vertical, horizontal + 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	public ArrayList<Block> map_n(ArrayList<Block> mapa, int vertical,
			int horizontal) {
		mapa.add(new Block(this.colour, vertical, horizontal));

		if (this.connDown != null) {
			mapa = connDown.mapD_nonchange(mapa, vertical + 1, horizontal);
		}
		if (this.connLeft != null) {
			mapa = connLeft.mapL_nonchange(mapa, vertical, horizontal - 1);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR_nonchange(mapa, vertical, horizontal + 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU_nonchange(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	private ArrayList<Block> mapU_nonchange(ArrayList<Block> mapa,
			int vertical, int horizontal) {
		mapa.add(new Block(this.colour, vertical, horizontal));

		if (this.connLeft != null) {
			mapa = connLeft.mapL(mapa, vertical, horizontal - 1);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR(mapa, vertical, horizontal + 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	private ArrayList<Block> mapR_nonchange(ArrayList<Block> mapa,
			int vertical, int horizontal) {
		mapa.add(new Block(this.colour, vertical, horizontal));

		if (this.connDown != null) {
			mapa = connDown.mapD(mapa, vertical + 1, horizontal);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR(mapa, vertical, horizontal + 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	private ArrayList<Block> mapL_nonchange(ArrayList<Block> mapa,
			int vertical, int horizontal) {
		mapa.add(new Block(this.colour, vertical, horizontal));

		if (this.connDown != null) {
			mapa = connDown.mapD(mapa, vertical + 1, horizontal);
		}
		if (this.connLeft != null) {
			mapa = connLeft.mapL(mapa, vertical, horizontal - 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	private ArrayList<Block> mapD_nonchange(ArrayList<Block> mapa,
			int vertical, int horizontal) {
		mapa.add(new Block(this.colour, vertical, horizontal));

		if (this.connDown != null) {
			mapa = connDown.mapD(mapa, vertical + 1, horizontal);
		}
		if (this.connLeft != null) {
			mapa = connLeft.mapL(mapa, vertical, horizontal - 1);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR(mapa, vertical, horizontal + 1);
		}

		return mapa;
	}

	private ArrayList<Block> mapU(ArrayList<Block> mapa, int vertical,
			int horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
		mapa.add(this);

		if (this.connLeft != null) {
			mapa = connLeft.mapL(mapa, vertical, horizontal - 1);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR(mapa, vertical, horizontal + 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	private ArrayList<Block> mapR(ArrayList<Block> mapa, int vertical,
			int horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
		mapa.add(this);

		if (this.connDown != null) {
			mapa = connDown.mapD(mapa, vertical + 1, horizontal);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR(mapa, vertical, horizontal + 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	private ArrayList<Block> mapL(ArrayList<Block> mapa, int vertical,
			int horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
		mapa.add(this);

		if (this.connDown != null) {
			mapa = connDown.mapD(mapa, vertical + 1, horizontal);
		}
		if (this.connLeft != null) {
			mapa = connLeft.mapL(mapa, vertical, horizontal - 1);
		}
		if (this.connUp != null) {
			mapa = connUp.mapU(mapa, vertical - 1, horizontal);
		}

		return mapa;
	}

	private ArrayList<Block> mapD(ArrayList<Block> mapa, int vertical,
			int horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
		mapa.add(this);

		if (this.connDown != null) {
			mapa = connDown.mapD(mapa, vertical + 1, horizontal);
		}
		if (this.connLeft != null) {
			mapa = connLeft.mapL(mapa, vertical, horizontal - 1);
		}
		if (this.connRight != null) {
			mapa = connRight.mapR(mapa, vertical, horizontal + 1);
		}

		return mapa;
	}

	public boolean checkGravity() {
		ArrayList<Block> mapa = map(new ArrayList<Block>(), vertical,
				horizontal);
		for (int i = 0; i < mapa.size(); i++) {
			if (mapa.get(i).connDown == null) {
				if (mapa.get(i).vertical != 15) {
					if (GameField.fieldFree[mapa.get(i).vertical + 1][mapa
							.get(i).horizontal]) {
					} else {
						falling = false;
						return false;
					}
				} else {
					falling = false;
					return false;
				}
			}
		}
		falling = true;
		return true;
	}

	public void pullDown() {
		ArrayList<Block> mapa = map(new ArrayList<Block>(), vertical,
				horizontal);
		for (int i = 0; i < mapa.size(); i++) {
			if (mapa.get(i).connUp == null) {
				GameField.fieldFree[mapa.get(i).vertical][mapa.get(i).horizontal] = true;
				mapa.get(i).vertical += 1;
				mapa.get(i).horizontal = mapa.get(i).horizontal;
				GameField.fieldFree[mapa.get(i).vertical][mapa.get(i).horizontal] = false;
			} else {
				mapa.get(i).vertical += 1;
				mapa.get(i).horizontal = mapa.get(i).horizontal;
				GameField.fieldFree[mapa.get(i).vertical][mapa.get(i).horizontal] = false;
			}

		}
	}

	public void flip() {
		if (isCenter) {
			ArrayList<Block> mapa = map(new ArrayList<Block>(), vertical,
					horizontal);
			Block temp = new Block();
			for (int i = 0; i < mapa.size(); i++) {

				temp = mapa.get(i).connRight;
				mapa.get(i).connRight = mapa.get(i).connUp;
				mapa.get(i).connUp = mapa.get(i).connLeft;
				mapa.get(i).connLeft = mapa.get(i).connDown;
				mapa.get(i).connDown = temp;

			}
		}

	}

	public boolean isOnTouch() {
		if (this.isCenter) {
			return this.onTouch;
		}
		
		ArrayList<Block> mapa = map(new ArrayList<Block>(), vertical,
				horizontal);

		for (int i = 0; i < mapa.size(); i++) {
			if (mapa.get(i).isCenter) {
				return mapa.get(i).onTouch;
			}
		}

		return true;
	}

	public void dispose() {
		this.img=null;
		connLeft = connRight = connUp = connDown = null;
		
	}

}
