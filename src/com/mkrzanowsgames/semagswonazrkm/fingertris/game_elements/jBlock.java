package com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements;

import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Image;
import com.mkrzanowsgames.semagswonazrkm.fingertris.game_elements.oneBlock.BlockState;
import com.mkrzanowsgames.semagswonazrkm.fingertris.start.GameScreen;

public class jBlock {

	private BlockState state;

	public Block center, left, leftUp, right, rightDown;

	public jBlock() {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour);
		left = new Block(colour);
		right = new Block(colour);
		leftUp = new Block(colour);
		rightDown = new Block(colour);
		center.connLeft = left;
		center.connRight = right;
		left.connRight = center;
		left.connUp = leftUp;
		leftUp.connDown = left;
		right.connDown = rightDown;
		right.connLeft = center;
		rightDown.connUp = right;
		state = BlockState.Ready;
	}

	public jBlock(int disp) {
		int colour = GameScreen.colourList.chooseColour();
		center = new Block(colour, disp);
		left = new Block(colour, disp);
		right = new Block(colour, disp);
		leftUp = new Block(colour, disp);
		rightDown = new Block(colour, disp);
		center.connLeft = left;
		center.connRight = right;
		left.connRight = center;
		left.connUp = leftUp;
		leftUp.connDown = left;
		right.connDown = rightDown;
		right.connLeft = center;
		rightDown.connUp = right;
		state = BlockState.Ready;
		
	}

	public jBlock(int disp, int x, int y) {
		int colour = GameScreen.colourList.chooseColour();
		Image img = GameScreen.chooseImg();
		center = new Block(colour, disp, x, y, true, img);
		left = new Block(colour, disp, x, y, false, img);
		right = new Block(colour, disp, x, y, false, img);
		leftUp = new Block(colour, disp, x, y, false, img);
		rightDown = new Block(colour, disp, x, y, false, img);
		center.connLeft = left;
		center.connRight = right;
		left.connRight = center;
		left.connUp = leftUp;
		leftUp.connDown = left;
		right.connDown = rightDown;
		right.connLeft = center;
		rightDown.connUp = right;
		state = BlockState.Ready;
		
	}

	public void paint(float deltaTime) {

	}

	public void update(float deltaTime) {

		switch (state) {
		case Ready:

			break;
		case OnField:

			break;
		case Destroyed:

			break;
		case Recycled:

			break;
		default:
			break;
		}

	}

}
