package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Data;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public class InstructionsScreen extends Screen {
	private int state;
	private Paint paint, text, pHiScore;
	private final int max = 9;

	public InstructionsScreen(Game game) {
		super(game);

		paint = new Paint();
		paint.setTextSize(50);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTypeface(Assets.Gi);

		text = new Paint();
		text.setTextSize(40);
		text.setTextAlign(Paint.Align.CENTER);
		text.setAntiAlias(true);
		text.setColor(Color.BLACK);
		text.setTypeface(Assets.Gi);

		pHiScore = new Paint();
		pHiScore.setTextSize(50);
		pHiScore.setTextAlign(Paint.Align.CENTER);
		pHiScore.setAntiAlias(true);
		pHiScore.setColor(Color.BLACK);
		pHiScore.setTypeface(Assets.Gi);

		state = 0;
	}

	@Override
	public void update(float deltaTime) {
		@SuppressWarnings("unused")
		Graphics g = game.getGraphics();
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		if (touchEvents != null) {
			int len = touchEvents.size();
			for (int i = 0; i < len; i++) {
				TouchEvent event = touchEvents.get(i);
				if (event.type == TouchEvent.TOUCH_UP) {
					if (inBounds(event, 490, 665, 300, 70)) {
						backButton();
						return;
					}
					if (inBounds(event, 640, 0, 640, 800)) {
						if (state < max - 1)
							state++;
						return;
					}
					if (inBounds(event, 0, 0, 640, 800)) {
						if (state > 0)
							state--;
						return;
					}
				}
			}
		}

	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();

		switch (state) {
		case 0:
			g.drawImage(Assets.mainMenu, 0, 0);
			g.drawString("Dear Sir or Madame", 640, 250, text);
			g.drawString("Welcome to Fingertris.", 640, 295, text);
			g.drawString("To start tap RIGHT side of the screen.", 820, 360,
					text);
			g.drawString(
					"To read again previous screen tap LEFT side of the screen.",
					560, 420, text);
			g.drawString("You can always go back to main menu,", 640, 500, text);
			g.drawString("either by tapping at the bottom of the screen,", 640,
					545, text);
			g.drawString("or pressing hardware 'Back' button.", 640, 590,
					text);
			break;

		default:
			g.drawImage(Assets.gameScreen, 0, 0);
			g.drawString("/", 910, 55, pHiScore);
			g.drawString("0", 1060, 55, pHiScore);

			g.drawString("0", 760, 55, pHiScore);

			g.drawString("Level:", 700, 340, pHiScore);
			g.drawString("1", 890, 340, pHiScore);

			g.drawRect(Data.pauseButtX + Data.buttSize / 2 - 25,
					Data.dispOffset + Data.buttSize / 2 - 25, Data.blockSize,
					Data.blockSize, Color.RED);
			g.drawRect(Data.pauseButtX + Data.buttSize / 2 - 25,
					Data.dispOffset + Data.buttSize / 2 - 75, Data.blockSize,
					Data.blockSize, Color.RED);
			g.drawRect(Data.pauseButtX + Data.buttSize / 2 - 75,
					Data.dispOffset + Data.buttSize / 2 - 25, Data.blockSize,
					Data.blockSize, Color.RED);
			g.drawImage(Assets.klocE, Data.pauseButtX + Data.buttSize / 2 - 25,
					Data.dispOffset + Data.buttSize / 2 - 25);
			g.drawImage(Assets.klocE, Data.pauseButtX + Data.buttSize / 2 - 75,
					Data.dispOffset + Data.buttSize / 2 - 25);
			g.drawImage(Assets.klocE, Data.pauseButtX + Data.buttSize / 2 - 25,
					Data.dispOffset + Data.buttSize / 2 - 75);

			g.drawRect(
					Data.pauseButtX + Data.buttSize / 2 + Data.buttSize - 25,
					Data.dispOffset + Data.buttSize / 2 - 25, Data.blockSize,
					Data.blockSize, Color.GREEN);
			g.drawRect(
					Data.pauseButtX + Data.buttSize / 2 + Data.buttSize - 25,
					Data.dispOffset + Data.buttSize / 2 - 75, Data.blockSize,
					Data.blockSize, Color.GREEN);
			g.drawRect(
					Data.pauseButtX + Data.buttSize / 2 + Data.buttSize - 25,
					Data.dispOffset + Data.buttSize / 2 + 25, Data.blockSize,
					Data.blockSize, Color.GREEN);
			g.drawRect(
					Data.pauseButtX + Data.buttSize / 2 + Data.buttSize + 25,
					Data.dispOffset + Data.buttSize / 2 + 25, Data.blockSize,
					Data.blockSize, Color.GREEN);
			g.drawImage(Assets.klocJ, Data.pauseButtX + Data.buttSize / 2
					+ Data.buttSize - 25, Data.dispOffset + Data.buttSize / 2
					- 25);
			g.drawImage(Assets.klocJ, Data.pauseButtX + Data.buttSize / 2
					+ Data.buttSize - 25, Data.dispOffset + Data.buttSize / 2
					- 75);
			g.drawImage(Assets.klocJ, Data.pauseButtX + Data.buttSize / 2
					+ Data.buttSize - 25, Data.dispOffset + Data.buttSize / 2
					+ 25);
			g.drawImage(Assets.klocJ, Data.pauseButtX + Data.buttSize / 2
					+ Data.buttSize + 25, Data.dispOffset + Data.buttSize / 2
					+ 25);

			g.drawRect(Data.pauseButtX + Data.buttSize / 2 + Data.buttSize * 2
					- 25, Data.dispOffset + Data.buttSize / 2 - 25,
					Data.blockSize, Data.blockSize, Color.YELLOW);
			g.drawRect(Data.pauseButtX + Data.buttSize / 2 + Data.buttSize * 2
					+ 25, Data.dispOffset + Data.buttSize / 2 - 25,
					Data.blockSize, Data.blockSize, Color.YELLOW);
			g.drawRect(Data.pauseButtX + Data.buttSize / 2 + Data.buttSize * 2
					- 75, Data.dispOffset + Data.buttSize / 2 - 25,
					Data.blockSize, Data.blockSize, Color.YELLOW);
			g.drawImage(Assets.klocD, Data.pauseButtX + Data.buttSize / 2
					+ Data.buttSize * 2 - 25, Data.dispOffset + Data.buttSize
					/ 2 - 25);
			g.drawImage(Assets.klocD, Data.pauseButtX + Data.buttSize / 2
					+ Data.buttSize * 2 + 25, Data.dispOffset + Data.buttSize
					/ 2 - 25);
			g.drawImage(Assets.klocD, Data.pauseButtX + Data.buttSize / 2
					+ Data.buttSize * 2 - 75, Data.dispOffset + Data.buttSize
					/ 2 - 25);

			pHiScore.setColor(Color.WHITE);
			pHiScore.setTextSize(60);
			g.drawString("2", 770, 630, pHiScore);
			pHiScore.setTextSize(50);
			pHiScore.setColor(Color.BLACK);
			g.drawImage(Assets.pause, Data.pauseButtX, Data.buttOffset);
			g.drawImage(Assets.forfeit_ina, Data.pauseButtX + Data.buttSize,
					Data.buttOffset);
			g.drawImage(Assets.next, Data.pauseButtX + 2 * Data.buttSize,
					Data.buttOffset);
			g.drawARGB(150, 255, 255, 255);

			switch (state) {
			case 1:
				g.drawString("This is the main screen of the game.", 640, 250,
						text);
				g.drawString("The goal is to get the highest score possible.",
						640, 300, text);
				g.drawString(
						"To achieve that You need to fill horizontal lines",
						640, 350, text);
				g.drawString("of the gray area on the left with blocks.", 640,
						400, text);
				g.drawString(
						"Once filled, the line will disappear and grant You points.",
						640, 450, text);
				g.drawString(
						"If a block that does not fit anywhere will be put on the field,",
						640, 500, text);
				g.drawString("the round will end.", 640, 550, text);
				break;
			case 2:
				g.drawString("The score is displayed here.", 840, 130, text);
				g.drawString("Nuber on the left is your current score,", 840,
						180, text);
				g.drawString("and number on the right is", 840, 230, text);
				g.drawString("the highest score recorded.", 840, 280, text);
				g.drawRect(620, 65, 550, 20, Color.BLUE);

				break;
			case 3:
				g.drawString("The blocks are stored here.", 350, 150, text);
				g.drawString("No more than 3 blocks can be here at once.", 370, 200,
						text);
				g.drawString("You can tap them to rotate clockwise,", 350, 250,
						text);
				g.drawString("or drag them on the field or into the bin.", 350,
						300, text);
				g.drawString("If put onto the field they cannot", 350, 350,
						text);
				g.drawString("be removed or rotated, but You can", 350, 400,
						text);
				g.drawString("move them to any free space", 350, 450,
						text);
				g.drawString("given an unobstructed path.", 350, 500, text);

				g.drawRect(Data.pauseButtX, Data.dispOffset, 550, 20,
						Color.BLUE);

				g.drawRect(Data.pauseButtX, Data.dispOffset + Data.buttSize
						- 25, 550, 20, Color.BLUE);

				break;
			case 4:
				g.drawRect(Data.pauseButtX, Data.dispOffset + Data.buttSize
						- 25, 560, 20, Color.BLUE);
				g.drawRect(Data.pauseButtX, Data.dispOffset + Data.buttSize
						- 25, 20, 550, Color.BLUE);
				g.drawRect(Data.pauseButtX + 550, Data.dispOffset
						+ Data.buttSize - 25, 20, 550, Color.BLUE);

				g.drawString("This is the bin.", 380, 150, text);
				g.drawString("The number on the left indicates", 380, 200, text);
				g.drawString("how many times it can be used.", 380, 250, text);
				g.drawString("Block dropped here will be removed", 380, 300,
						text);
				g.drawString("if available, decreasing the number of uses.",
						380, 350, text);
				g.drawString("Uses will recover slowly overtime.", 380, 400,
						text);
				break;
			case 5:
				g.drawString("This is the timer.", 350, 150, text);
				g.drawString("When the line fills red", 350, 200, text);
				g.drawString("a new block will be spawned.", 350, 250,
						text);
				g.drawString("Blocks will spawn below,", 350, 300, text);
				g.drawString("or on the field if there are", 350, 350, text);
				g.drawString("already three blocks present.", 350, 400, text);

				g.drawRect(620, 280, 570, 20, Color.BLUE);

				break;
			case 6:
				g.drawRect(620, 280, 570, 20, Color.BLUE);
				g.drawRect(620, 350, 570, 20, Color.BLUE);

				g.drawString("This is current difficulty.", 350, 150, text);
				g.drawString("The lowest is 1, the highest 10.", 350, 200, text);
				g.drawString("Difficulty will increase with score.", 350, 250,
						text);
				g.drawString("More points are gained", 350, 300, text);
				g.drawString("at higher difficulty, but", 350, 350, text);
				g.drawString("also the time runs faster.", 350, 400, text);
				break;
			case 7:
				g.drawString("Those are the menu buttons.", 350, 150, text);
				g.drawString("From left to right:", 350, 200, text);
				g.drawString("'pause/unpause', 'forfeit', 'next'.", 350, 250, text);
				g.drawString("'Forfeit' button is active", 350, 300, text);
				g.drawString("only during pause.", 350, 350, text);
				g.drawString("'Next' button will spawn a new block", 350, 400,
						text);
				g.drawString("and reset the timer, also providing", 350, 450, text);
				g.drawString("a small amount of points.", 350, 500, text);
				g.drawString("However, it will not work", 350, 550, text);
				g.drawString("if there are three blocks below.", 350, 600, text);

				g.drawRect(620, 65, 570, 20, Color.BLUE);
				g.drawRect(620, 65 + Data.buttSize - 30, 570, 20, Color.BLUE);

				break;

			default:
				g.drawImage(Assets.mainMenu, 0, 0);
				g.drawString("You can start at any level of difficulty,", 640,
						250, text);
				g.drawString(
						"but it is best to start at 1 and progress further.",
						640, 300, text);
				g.drawString(
						"Do not forget to type your name after achieving high score.",
						640, 350, text);
				g.drawString("Good luck, have fun,", 640, 450, text);
				g.drawString("and thank You for playing.", 640, 500, text);

				break;
			}
		}

		g.drawString(String.valueOf(state + 1) + "/" + String.valueOf(max),
				1170, 65, paint);
		g.drawString("Back", 640, 710, paint);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void backButton() {
		game.setScreen(new MainMenuScreen(game));

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub

	}

}
