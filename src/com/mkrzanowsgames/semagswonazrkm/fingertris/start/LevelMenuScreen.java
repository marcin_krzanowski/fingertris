package com.mkrzanowsgames.semagswonazrkm.fingertris.start;

import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;

import com.mkrzanowsgames.semagswonazrkm.fingertris.data.Assets;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Game;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Graphics;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Screen;
import com.mkrzanowsgames.semagswonazrkm.fingertris.framework.Input.TouchEvent;

public class LevelMenuScreen extends Screen {
	private Paint paint, title;

	public LevelMenuScreen(Game game) {
		super(game);

		paint = new Paint();
		paint.setTextSize(50);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setTypeface(Assets.Gi);

		title = new Paint();
		title.setTextSize(60);
		title.setTextAlign(Paint.Align.CENTER);
		title.setAntiAlias(true);
		title.setColor(Color.BLACK);
		title.setTypeface(Assets.Gi);
	}

	@Override
	public void update(float deltaTime) {
		@SuppressWarnings("unused")
		Graphics g = game.getGraphics();

		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		if (touchEvents != null) {
			int len = touchEvents.size();
			for (int i = 0; i < len; i++) {
				TouchEvent event = touchEvents.get(i);
				if (event.type == TouchEvent.TOUCH_UP) {
					if (inBounds(event, 365, 165, 250, 70)) {
						Fingertris.data.setLevel(0);
						game.setScreen(new GameScreen(game));
						return;
					}
					if (inBounds(event, 665, 165, 250, 70)) {
						Fingertris.data.setLevel(1);
						game.setScreen(new GameScreen(game));
						return;
					}
					if (inBounds(event, 365, 265, 250, 70)) {
						Fingertris.data.setLevel(2);
						game.setScreen(new GameScreen(game));
						return;

					}
					if (inBounds(event, 665, 265, 250, 70)) {
						Fingertris.data.setLevel(3);
						game.setScreen(new GameScreen(game));
						return;

					}
					if (inBounds(event, 365, 365, 250, 70)) {
						Fingertris.data.setLevel(4);
						game.setScreen(new GameScreen(game));
						return;

					}
					if (inBounds(event, 665, 365, 250, 70)) {
						Fingertris.data.setLevel(5);
						game.setScreen(new GameScreen(game));
						return;

					}
					if (inBounds(event, 365, 465, 250, 70)) {
						Fingertris.data.setLevel(6);
						game.setScreen(new GameScreen(game));
						return;

					}
					if (inBounds(event, 665, 465, 250, 70)) {
						Fingertris.data.setLevel(7);
						game.setScreen(new GameScreen(game));
						return;

					}
					if (inBounds(event, 365, 565, 250, 70)) {
						Fingertris.data.setLevel(8);
						game.setScreen(new GameScreen(game));
						return;

					}
					if (inBounds(event, 665, 565, 250, 70)) {
						Fingertris.data.setLevel(9);
						game.setScreen(new GameScreen(game));
						return;
					}
					if (inBounds(event, 490, 665, 300, 70)) {
						backButton();
						return;
					}
				}
			}
		}
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.drawImage(Assets.levelMenu, 0, 0);

		g.drawString("Choose starting difficulty:", 640, 110, title);

		g.drawString("1", 480, 220, title);
		g.drawString("3", 480, 320, title);
		g.drawString("5", 480, 420, title);
		g.drawString("7", 480, 520, title);
		g.drawString("9", 480, 620, title);

		g.drawString("2", 800, 220, title);
		g.drawString("4", 800, 320, title);
		g.drawString("6", 800, 420, title);
		g.drawString("8", 800, 520, title);
		g.drawString("10", 800, 620, title);

		g.drawString("Back", 640, 710, paint);

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {
		game.setScreen(new MainMenuScreen(game));

	}

	@Override
	public void keyPressed(char c) {
		// TODO Auto-generated method stub

	}

}
